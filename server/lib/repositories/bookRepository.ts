import * as mongoose from 'mongoose';
import { IBookModel } from '../../../shared/models/book.model';

interface IBookEntity extends IBookModel, mongoose.Document { }
const ObjectId = mongoose.Schema.Types.ObjectId
mongoose.model('Book', new mongoose.Schema({
  title: String,
  authors: [ObjectId],
  magazine: ObjectId,
  img: String
}));
export const BookRepository = mongoose.model<IBookEntity>('Book');  

