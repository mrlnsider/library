import * as mongoose from 'mongoose';
import { IAuthorModel } from '../../../shared/models/author.model';

interface IAuthorEntity extends IAuthorModel, mongoose.Document { }
mongoose.model('Author', new mongoose.Schema({
    name: String
}));
export const AuthorRepository = mongoose.model<IAuthorEntity>('Author');
