import * as mongoose from 'mongoose';
import { IMagazineModel } from '../../../shared/models/magazine.model';

interface IMagazineEntity extends IMagazineModel, mongoose.Document { }
mongoose.model('Magazine', new mongoose.Schema({
    title: String,
    publisher: String,
    img: String
}));
export const MagazineRepository = mongoose.model<IMagazineEntity>('Magazine');