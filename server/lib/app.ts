import * as express from "express";
import * as bodyParser from "body-parser";
import { Routes } from "./routes/routes";
import * as mongoose from "mongoose";
import * as cors from "cors";
import { environment } from "./environment/environment";
import * as passport from "passport";
import { GooglePassport } from "./google/passport";

class App {

    public app: express.Application;
    public routePrv: Routes = new Routes();
    public mongoUrl: string = environment().mongoDbConnectionString;
    public googlePassport = GooglePassport;

    constructor() {
        this.app = express();
        this.app.use(cors());
        this.config();             
        this.mongoSetup();
        this.googlePassport(passport);
        this.routePrv.routes(this.app);
    }

    private config(): void{
        this.app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
        this.app.use(bodyParser.json({limit: '50mb'}));
        // serving static files 
        this.app.use(express.static('public'));
        this.app.use(passport.initialize());
        this.app.use(passport.session());
    }

    private mongoSetup(): void{
        mongoose.connect(this.mongoUrl,  { useNewUrlParser: true });        
    }

}

export default new App().app;