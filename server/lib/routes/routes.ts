import { Request, Response } from "express";
import { BookController } from "../controllers/bookController";
import { AuthorController } from "../controllers/authorController";
import { AuthMiddleware } from "../middlewares/authMiddleware";
import { AdminController } from "../controllers/adminController"
import { AuthController } from "../controllers/authController"
import { UserRole } from "../../../shared/models/enam/userRole.enam";
import { MagazineController } from "../controllers/magazineController";
import * as passport from "passport";
import { GooglePassport } from "../google/passport";
import { UserController } from "../controllers/userController";

export class Routes {

    public bookController: BookController = new BookController()
    public authorController: AuthorController = new AuthorController()
    public authController: AuthController = new AuthController()
    public adminController: AdminController = new AdminController()
    public magazineController: MagazineController = new MagazineController()
    public userController: UserController = new UserController()

    public routes(app): void {

        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'GET request successfulll!!!!'
                })
            })

        // Auth
        app.route('/auth/profile')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.authController.profile);
        app.route('/auth/register')
            .post(this.authController.register);
        app.route('/auth/login')
            .post(this.authController.login);
        app.route('/auth/google')
            .get(passport.authenticate('google', {
                scope: ['profile', 'email']
            }));
        app.route('/auth/google/callback')
            .get(passport.authenticate('google', {
                successRedirect : '/auth/profile',
                failureRedirect : '/auth/login'
            }));

        // Admin
        app.route('/admin/users')
            .get(AuthMiddleware([UserRole.admin]), this.adminController.getConfirm)
            .post(AuthMiddleware([UserRole.admin]), this.adminController.add)
        app.route('/admin/users/:id')
            .put(AuthMiddleware([UserRole.admin]), this.adminController.update)
            .delete(AuthMiddleware([UserRole.admin]), this.adminController.delete)
        app.route('/admin/confirm')
            .get(AuthMiddleware([UserRole.admin]), this.adminController.getNoConfirm)

        // User
        app.route('/user/:id')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.userController.getById)
            .put(AuthMiddleware([UserRole.user, UserRole.admin]), this.userController.update)
        app.route('/user/pass/:id')
            .put(AuthMiddleware([UserRole.user, UserRole.admin]), this.userController.updatePass)

        // Book 
        app.route('/book')
            .post(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.findBooks)
        app.route('/book/add')
            .post(AuthMiddleware([UserRole.admin]), this.bookController.add)
        app.route('/book/:bookId')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.getById)
            .put(AuthMiddleware([UserRole.admin]), this.bookController.update)
            .delete(AuthMiddleware([UserRole.admin]), this.bookController.delete)
        app.route('/book/get/:authorId')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.bookController.getByAuthorId)

        // Author 
        app.route('/author')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.authorController.get)
            .post(AuthMiddleware([UserRole.admin]), this.authorController.add);
        app.route('/author/:authorId')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.authorController.getById)
            .put(AuthMiddleware([UserRole.admin]), this.authorController.update)
            .delete(AuthMiddleware([UserRole.admin]), this.authorController.delete)

        // Magazine 
        app.route('/magazine')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.magazineController.get)
            .post(AuthMiddleware([UserRole.admin]), this.magazineController.add);
        app.route('/magazine/:id')
            .get(AuthMiddleware([UserRole.user, UserRole.admin]), this.magazineController.getById)
            .put(AuthMiddleware([UserRole.admin]), this.magazineController.update)
            .delete(AuthMiddleware([UserRole.admin]), this.magazineController.delete)
    }
}