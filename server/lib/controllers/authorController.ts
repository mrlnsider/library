import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { AuthorRepository } from '../repositories/authorRepository';
import { BookRepository } from '../repositories/bookRepository';

export class AuthorController{

    public get (req: RequestModel<{}>, res: Response) {
        AuthorRepository.find({}, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }
    
    public getById (req: RequestModel<{authorId: number}>, res: Response) {           
        AuthorRepository.findById(req.params.authorId, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public add (req: RequestModel<{}>, res: Response) {
        let newAuthor = new AuthorRepository(req.body);
        newAuthor.save((err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public update (req: RequestModel<{authorId: number}>, res: Response) {
        AuthorRepository.findOneAndUpdate({ _id: req.params.authorId }, req.body, { new: true }, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        })         
    }

    public delete (req: RequestModel<{authorId: number}>, res: Response) {
        BookRepository.updateMany({ authors: req.params.authorId }, { $pull: { 'authors': req.params.authorId}}, (err, book) => { 
            if (err) return res.status(500).send('Error on the server.'); 
            AuthorRepository.remove({ _id: req.params.authorId }, (err) => {
                if (err) return res.status(500).send('Error on the server.');
                res.json({ message: 'Successfully deleted author!'});
            });
        })
    }    
}