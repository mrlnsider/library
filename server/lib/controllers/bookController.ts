import { Response } from 'express';
import { RequestModel } from '../middlewares/authMiddleware'
import { BookRepository } from '../repositories/bookRepository';
import * as mongoose from 'mongoose';

export class BookController {

    public findBooks(req: RequestModel<{}>, res: Response) {
        const searchName = { $regex: ".*" + req.body.bookName + ".*", $options: 'i' }
        const length = BookRepository.aggregate([
            {
                "$lookup":
                {
                    from: 'authors',
                    localField: 'authors',
                    foreignField: '_id',
                    as: 'authors'
                }
            },
            {
                "$match":
                {
                    $or:
                        [{ title: searchName }, { "authors.name": searchName }]
                }
            },
            
        ])
        .count('length')
        .exec()

        const books = BookRepository.aggregate([
            {
                "$lookup":
                {
                    from: 'authors',
                    localField: 'authors',
                    foreignField: '_id',
                    as: 'authors'
                }
            },
            {
                "$lookup":
                {
                    from: 'magazines',
                    localField: 'magazine',
                    foreignField: '_id',
                    as: 'magazine'
                }
            },
            {
                "$match":
                {
                    $or:
                        [{ title: searchName }, { "authors.name": searchName }]
                }
            }
        ])
        .skip(req.body.itemsPerPage * (req.body.currentPage - 1))
        .limit(req.body.itemsPerPage)
        .exec()
        Promise.all([length, books]).then( result => res.json({bookslength: result[0], books: result[1] }))
    }

    public getByAuthorId(req: RequestModel< {authorId: number}>, res: Response) {
        let authorId = req.params.authorId;
        BookRepository.find({ authors: authorId }, (err, book) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public getById(req: RequestModel<{ bookId: string }>, res: Response) {
        var id = mongoose.Types.ObjectId(req.params.bookId);
        BookRepository.aggregate([
            {
                "$match":
                    { "_id": id }

            },
            {
                "$lookup":
                {
                    from: 'authors',
                    localField: 'authors',
                    foreignField: '_id',
                    as: 'authors'
                }
            },
            {
                "$lookup":
                {
                    from: 'magazines',
                    localField: 'magazine',
                    foreignField: '_id',
                    as: 'magazine'
                }
            },
        ])
            .exec((err, resulting) => {
                if (err) res.send('');
                res.json(resulting);
                res.end();
            })
    }

    public add(req: RequestModel<{}>, res: Response) {
        let newBook = new BookRepository(req.body);
        newBook.save((err, book) => {
            const Schema = mongoose.Schema;
            let ObjectId = Schema.Types.ObjectId;
            req.body.author = ObjectId;
            if (req.body.magazine) {
                req.body.magazine = ObjectId;
            }
            if (err) return res.status(500).send('Error on the server.');
            res.json(book);
        });
    }

    public update(req: RequestModel<{ bookId: string }>, res: Response) {
        BookRepository.findOneAndUpdate({ _id: req.params.bookId }, req.body, { new: true }, (err, contact) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(contact);
        });
    }

    public delete(req: RequestModel<{ bookId: string }>, res: Response) {
        BookRepository.remove({ _id: req.params.bookId }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted contact!' });
        });
    }

}