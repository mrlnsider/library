import { RequestModel } from '../middlewares/authMiddleware';
import { Response } from 'express';
import { MagazineRepository } from '../repositories/magazineRepository';
import { BookRepository } from '../repositories/bookRepository';

export class MagazineController {

    public get (req: RequestModel<{}>, res: Response) {
        MagazineRepository.find({}, (err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(magazine);
        });
    }
    
    public getById (req: RequestModel<{id: number}>, res: Response) {           
        MagazineRepository.findById(req.params.id, (err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(magazine);
        });
    }

    public add (req: RequestModel<{}>, res: Response) {
        let newMagazine = new MagazineRepository(req.body);
        newMagazine.save((err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(magazine);
        });
    }

    public update (req: RequestModel<{id: number}>, res: Response) {           
        MagazineRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, magazine) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(magazine);
        });
    }

    public delete (req: RequestModel<{id: number}>, res: Response) {
        BookRepository.updateMany({ magazine: req.params.id }, { $pull: { 'magazine': req.params.id}}, (err, book) => { 
            if (err) return res.status(500).send('Error on the server.');       
            MagazineRepository.remove({ _id: req.params.id }, (err) => {
                if (err) return res.status(500).send('Error on the server.');
                res.json({ message: 'Successfully deleted magazine!'});
            });
        });
    }
}