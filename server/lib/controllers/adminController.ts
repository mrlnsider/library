import { RequestModel, RequestPost } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import * as jsonwebtoken from "jsonwebtoken";
import { authConfig } from "../config"
import { UserRepository } from '../repositories/userRepository';
import { UserRole } from '../../../shared/models/enam/userRole.enam';


export class AdminController {

    public getConfirm(req:  RequestModel<{}>, res: Response) {
        UserRepository.find({confirm: true}, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        })
    }

    public getNoConfirm (req:  RequestModel<{}>, res: Response) {
        UserRepository.find({confirm: false}, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        })
    }

    public add(req: RequestPost<{ fullName: string, email: string, password: string, confirm: boolean ,role?: number }>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
        UserRepository.create({
            fullName: req.body.fullName,
            email: req.body.email,
            password: hashedPassword,
            confirm: req.body.confirm,
            role: req.body.role
        },
        (err, user) => {
            if (err) return res.status(500).send("There was a problem registering the user.")
            // create a token
            const userData = { 
                id: user._id, 
                fullName: user.fullName, 
                email: user.email, 
                confirm: user.confirm, 
                role: UserRole.user 
            }
            var token = jsonwebtoken.sign(userData, authConfig.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            res.status(200).send({ auth: true, token: token, user: user });
        });
    }
    
    public update (req: RequestModel<{id: string}>, res: Response) {
        if (req.body.password) {
            var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
            req.body.password = hashedPassword;
        }
        UserRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        }); 
    }

    public delete (req: RequestModel<{id: string}>, res: Response) {           
        UserRepository.remove({ _id: req.params.id }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted user!'});
        });
    }

}
