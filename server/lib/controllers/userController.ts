import { RequestModel } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import { UserRepository } from '../repositories/userRepository';

export class UserController {
    
    public getById(req: RequestModel<{id: number}>, res: Response) {           
        UserRepository.findById(req.params.id, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }

    public update(req: RequestModel<{id: number}>, res: Response) {           
        UserRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }

    public updatePass(req: RequestModel<{id: number}>, res: Response) {
        const body = {
            password: bcryptjs.hashSync(req.body.newPass, 8)
        }
        UserRepository.findById(req.params.id, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            var passwordIsValid = bcryptjs.compareSync(req.body.oldPass, user.password);
            if (!passwordIsValid) return res.status(400).send('Incorrect password');
            UserRepository.findOneAndUpdate({ _id: req.params.id }, body, { new: true }, (err, user) => {
                if (err) return res.status(500).send('Error on the server.');
                res.json(user);
            });
        });
    }
}