import { RequestModel, RequestPost } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import * as jsonwebtoken from "jsonwebtoken";
import { authConfig } from "../config"
import { UserRepository } from '../repositories/userRepository';
import { UserRole } from '../../../shared/models/enam/userRole.enam';


export class AuthController {

    public register(req: RequestPost<{ fullName: string, email: string, password: string, confirm: boolean }>, res: Response) {
        UserRepository.findOne({ email: req.body.email }, (err, user) => {
            if (user) return res.status(400).send('This email is already registered');
            var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
            UserRepository.create({
                fullName: req.body.fullName,
                email: req.body.email,
                password: hashedPassword,
                confirm: req.body.confirm,
                role: UserRole.user
            },
            (err, user) => {
                if (err) return res.status(500).send("There was a problem registering the user.")
                res.status(200).send({ reg: true});
            });
        });
    }

    public login(req: RequestPost<{ login: string, password: string }>, res: Response) {
        console.log(req.body)
        UserRepository.findOne({ email: req.body.login }, (err, user) => {
            if (err) return res.status(500).send('Error on the server, sorry!');
            if (!user) return res.status(401).send('No user found, please register!');
            var passwordIsValid = bcryptjs.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(400).send('Incorrect password');
            if (user.confirm === false) return res.status(400).send('User not confirmed yet, please wait!');
            // create a token
            var token = jsonwebtoken.sign({ id: user._id, fullName: user.fullName, confirm: user.confirm, email: user.email, role: user.role }, authConfig.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            res.status(200).send({ auth: true, token: token, user: user });
        });
    }

    public profile(req: RequestModel<{}>, res: Response) {
        console.log(req.body)
        res.status(200).send(req.user);
    }

    public googleAuth (req: RequestModel<{}>, res: Response) {
        res.status(200).send('auth');
    }
}
