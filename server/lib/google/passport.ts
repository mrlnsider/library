import { Strategy } from 'passport-google-oauth20';
import { googleKey } from '../google/key';
import { UserRepository } from '../repositories/userRepository';
import { AuthController } from '../controllers/authController';

export const GooglePassport = (passport) => {
    passport.use(
        new Strategy({
            clientID: googleKey.googleClientID,
            clientSecret: googleKey.googleClientSecret,
            callbackURL: '/auth/google/callback',
            proxy: true
        }, (accessToken, refreshToken, profile, done) => {
                console.log(accessToken, profile)
//                let a = new AuthController()
//a.register(, null)
                // done(null, profile => {
                //     let imageUrl = '';
                //     if (profile.photos && profile.photos.length) {
                //       imageUrl = profile.photos[0].value;
                //     }
                //     return {
                //         _id: profile.id,
                //         displayName: profile.displayName,
                //         image: imageUrl,
                //         confirm: true,        
                //         role:2
                //     };
                //   });
            }
        )
    )
}
