import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthService } from './shared/services/auth.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthGuardsService } from './shared/guards/auth-guards.service';
import { LocalStorageService } from './shared/services/local-storage.service';
import { AuthorService } from './shared/services/author.service';
import { AdminService } from './shared/services/admin.service';
import { BooksService } from './shared/services/books.service';
import { MagazineService } from './shared/services/magazine.service';
import { HeadersInterceptor } from './shared/interceptor/http-headers.interceptor';
import { UserService } from './shared/services/user.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { TestComponent } from './app/componensts/test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,
    AuthGuardsService,
    LocalStorageService,
    AuthorService,
    AdminService,
    BooksService,
    MagazineService,
    UserService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeadersInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
