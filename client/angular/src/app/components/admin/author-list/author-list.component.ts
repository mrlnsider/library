import { Component, OnInit } from '@angular/core';
import { IAuthorModel } from '../../../../../../../shared/models/author.model';
import { AuthorService } from 'src/app/shared/services/author.service';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { DeletePopupComponent } from '../popup/delete-popup/delete-popup.component';

@Component({
  selector: 'author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.scss']
})
export class AuthorListComponent implements OnInit {
  public authors: IAuthorModel[];
  public author: IAuthorModel;
  public toggleAdd: boolean;
  public currentId: string;

  constructor( private authorService: AuthorService, public dialog: MatDialog) { }

  ngOnInit() {
    this.toggleAdd = false;
    if (!this.author) {
      this.author = {
        _id: '',
        name: ''
      };
    }
    this.loadAuthors();
  }

  public loadAuthors(): void {
    this.authorService.getAuthors().subscribe(
      data => {
        this.authors = data as IAuthorModel[];
      }
    );
  }

  public toggleShowAdd(): void {
    this.toggleAdd = !this.toggleAdd;
  }

  public toggleShowEdit(id: string): void {
    this.loadAuthors();
    this.currentId = id;
  }

  public closeEdit(): void {
    this.loadAuthors();
    this.currentId = null;
  }

  public addAuthor(): void {
    this.authorService.addAuthor(this.author).subscribe( () => {
      this.author.name = null;
      this.loadAuthors();
    });
  }

  public editAuthor(author: IAuthorModel): void {
    this.authorService.updateAuthor(author).subscribe( () => {
      this.loadAuthors();
      this.currentId = null;
    });
  }

  public deleteAuthor(id: string, name: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      type: 'author',
      value: name,
    };
    const dialogRef = this.dialog.open(DeletePopupComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.authorService.deleteAuthor(id).subscribe( () => {
          this.loadAuthors();
        });
      }
    });
  }

}
