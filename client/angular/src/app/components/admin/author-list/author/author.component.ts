import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BooksService } from 'src/app/shared/services/books.service';
import { IBookModel } from '../../../../../../../../shared/models/book.model';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';
import { AuthorService } from 'src/app/shared/services/author.service';

@Component({
  selector: 'author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss']
})
export class AuthorComponent implements OnInit {
  public idBook: string;
  public author: IAuthorModel;
  public books: IBookModel[];

  constructor(private route: ActivatedRoute, private booksService: BooksService, private authorService: AuthorService) {
    this.books = [];
  }

  ngOnInit() {
    if (!this.author) {
      this.author = {
        _id: '',
        name: ''
      };
    }
    this.getPageId();
    this.getBooksByAuthor();
  }

  public getPageId(): void {
    this.route.params.subscribe(params => {
      this.idBook = params['id'];
    });
  }

  public getBooksByAuthor(): void {
    this.authorService.getAuthor(this.idBook).subscribe( data => {
      this.author = data as IAuthorModel;
      this.booksService.getBookByAuthor(this.author._id).subscribe( books => {
        this.books = books as IBookModel[];
      });
    });
  }

}
