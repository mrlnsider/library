import { Component, OnInit } from '@angular/core';
import { IMagazineModel } from '../../../../../../../shared/models/magazine.model';
import { MagazineService } from 'src/app/shared/services/magazine.service';
import { DeletePopupComponent } from '../popup/delete-popup/delete-popup.component';
import { MatDialogConfig, MatDialog } from '@angular/material';

@Component({
  selector: 'admin-magazines',
  templateUrl: './admin-magazines.component.html',
  styleUrls: ['./admin-magazines.component.scss']
})
export class AdminMagazinesComponent implements OnInit {

  public magazines: IMagazineModel[];
  public magazine: IMagazineModel;
  public showPopup: boolean;
  public btnName: string;

  constructor(private magazineService: MagazineService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.loadMagazines();
  }

  public loadMagazines(): void {
    this.magazineService.getMagazines().subscribe(
      data => {
        this.magazines = data as IMagazineModel[];
      }
    );
  }

  public addMagazine(): void {
    this.magazine = null;
    this.showPopup = true;
    this.btnName = 'Add';
  }

  public editMagazine(magazine: IMagazineModel): void {
    this.magazine = magazine;
    this.showPopup = true;
    this.btnName = 'Edit';
  }

  public deleteMagazine(id: string, title: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      type: 'magazine',
      value: title,
    };
    const dialogRef = this.dialog.open(DeletePopupComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.magazineService.deleteMagazine(id).subscribe( data => {
          this.loadMagazines();
        });
      }
    });
  }

  public closePopup(event: boolean): void {
    this.showPopup = event;
    this.loadMagazines();
  }

}
