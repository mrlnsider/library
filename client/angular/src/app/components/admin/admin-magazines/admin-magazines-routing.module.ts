import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminMagazinesComponent } from './admin-magazines.component';

const routes: Routes = [{
  path: '',
  component: AdminMagazinesComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminMagazinesRoutingModule {}
