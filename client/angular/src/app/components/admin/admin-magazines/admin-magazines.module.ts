import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminMagazinesComponent } from './admin-magazines.component';
import { AdminMagazinesRoutingModule } from './admin-magazines-routing.module';
import { MagazinePopupComponent } from '../popup/magazine-popup/magazine-popup.component';

@NgModule({
    declarations: [
        AdminMagazinesComponent,
        MagazinePopupComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        AdminMagazinesRoutingModule
    ]
})
export class AdminMagazinesModule {}
