import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { AuthorListComponent } from './author-list/author-list.component';
import { AuthorComponent } from './author-list/author/author.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: '',
        component: ConfirmComponent
      },
      {
        path: 'author',
        component: AuthorListComponent
      },
      {
        path: 'author/:id',
        component: AuthorComponent
      },
      {
        path: 'users',
        loadChildren: './users-list/users-list.module#UserListModule',
      },
      {
        path: 'books',
        loadChildren: './admin-books/admin-books.module#AdminBooksModule',
      },
      {
        path: 'magazines',
        loadChildren: './admin-magazines/admin-magazines.module#AdminMagazinesModule',
      }
    ]
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
