import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { AdminHeaderComponent } from 'src/app/containers/admin-header/admin-header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthorListComponent } from './author-list/author-list.component';
import { AuthorComponent } from './author-list/author/author.component';
import { DeletePopupComponent } from './popup/delete-popup/delete-popup.component';
import { MatButtonModule, MatIconModule, MatCardModule, MatMenuModule, MatDialogModule } from '@angular/material';


@NgModule({
    declarations: [
        AdminComponent,
        AdminHeaderComponent,
        ConfirmComponent,
        AuthorListComponent,
        AuthorComponent,
        DeletePopupComponent
    ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        MatMenuModule,
        MatDialogModule,
    ],
    entryComponents: [DeletePopupComponent]
})
export class AdminModule {}
