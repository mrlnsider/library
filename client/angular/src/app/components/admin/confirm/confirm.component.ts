import { Component, OnInit } from '@angular/core';
import { IUserModel } from '../../../../../../../shared/models/user.model';
import { AdminService } from 'src/app/shared/services/admin.service';
import { DeletePopupComponent } from '../popup/delete-popup/delete-popup.component';
import { MatDialogConfig, MatDialog } from '@angular/material';
import { BooksService } from 'src/app/shared/services/books.service';
import { MagazineService } from 'src/app/shared/services/magazine.service';
import { IMagazineModel } from '../../../../../../../shared/models/magazine.model';

@Component({
  selector: 'confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  public users: IUserModel[];
  public noUser: boolean;
  public amountUsers: number;
  public magaz = [];

  constructor( private adminService: AdminService, public dialog: MatDialog, private magazineService: MagazineService) {
    this.noUser = false;
  }

  ngOnInit() {
    this.loadUsers();
    this.loadMagazines();
  }

  public loadUsers(): void {
    this.adminService.getNoConfirmUsers().subscribe (data => {
      this.users = data as IUserModel[];
      this.amountUsers = this.users.length;
      if (this.users.length < 1) {
        this.noUser = true;
      }
    });
  }

  public loadMagazines(): void {
    this.magazineService.getMagazines().subscribe ((data: any) => {
      data = [].map.call(data, (item: IMagazineModel) =>  {
        return {
          title: item.title,
          publisher: item.publisher
        };
      });
      console.log('asdas', data);
    });
  }

  public confirmUser(id: string): void {
    this.adminService.updateConfirmUser(id).subscribe ( data => {
      this.loadUsers();
    });
  }

  public deleteUser(id: string, email: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      type: 'user',
      value: email,
    };
    const dialogRef = this.dialog.open(DeletePopupComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.adminService.deleteUser(id).subscribe( data => {
          this.loadUsers();
        });
      }
    });
  }

}
