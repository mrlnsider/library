import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { UserListRoutingModule } from './users-list-routing.module';
import { UsersListComponent } from './users-list.component';
import { UserPopupComponent } from '../popup/user-popup/user-popup.component';

@NgModule({
    declarations: [
        UsersListComponent,
        UserPopupComponent
    ],
    imports: [
        CommonModule,
        UserListRoutingModule,
        ReactiveFormsModule
    ]
})
export class UserListModule {}
