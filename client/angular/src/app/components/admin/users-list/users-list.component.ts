import { Component, OnInit } from '@angular/core';
import { UserRole } from 'src/app/shared/enum/userRole.enum';
import { IUserModel } from '../../../../../../../shared/models/user.model';
import { AdminService } from 'src/app/shared/services/admin.service';
import { UserService } from 'src/app/shared/services/user.service';
import { IUserInfoModel } from 'src/app/shared/models/userInfo.model';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { DeletePopupComponent } from '../popup/delete-popup/delete-popup.component';

@Component({
  selector: 'users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  public users: IUserModel[];
  public showPopup: boolean;
  public user: IUserModel;
  public btnName: string;
  public userRole = UserRole;
  public adminInfo: IUserInfoModel;

  constructor(private adminService: AdminService, private userService: UserService, public dialog: MatDialog) {
    this.showPopup = false;
  }

  ngOnInit() {
    this.loadAdminInfo();
    this.loadUsers();
  }

  public loadUsers(): void {
    this.adminService.getConfirmUsers().subscribe(
      data => {
        this.users = data as IUserModel[];
      }
    );
  }

  public loadAdminInfo(): void {
    this.userService.getUserInfo().subscribe(
      data => {
        this.adminInfo = data as IUserInfoModel;
      }
    );
  }

  public addUser(): void {
    this.user = null;
    this.showPopup = true;
    this.btnName = 'Add';

  }

  public editUser(user: IUserModel): void {
    this.user = user;
    this.showPopup = true;
    this.btnName = 'Edit';
  }

  public deleteUser(user: IUserModel, id: string): void {
    if (this.adminInfo['id'] === id) {
      console.log(`You can't delete yourself`);
      return;
    }
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      type: 'user',
      value: user.email,
    };
    const dialogRef = this.dialog.open(DeletePopupComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.adminService.deleteUser(id).subscribe( data => {
          this.loadUsers();
        });
      }
    });
  }

  public closePopup(event: boolean): void {
    this.showPopup = event;
    this.loadUsers();
  }

}
