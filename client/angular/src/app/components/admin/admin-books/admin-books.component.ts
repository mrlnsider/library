import { Component, OnInit } from '@angular/core';
import { IBookModel } from '../../../../../../../shared/models/book.model';
import { BooksService } from 'src/app/shared/services/books.service';
import { DeletePopupComponent } from '../popup/delete-popup/delete-popup.component';
import { MatDialog, MatDialogConfig } from '@angular/material';

@Component({
  selector: 'admin-books',
  templateUrl: './admin-books.component.html',
  styleUrls: ['./admin-books.component.scss']
})
export class AdminBooksComponent implements OnInit {
  public books: IBookModel[];
  public book: IBookModel;
  public showPopup: boolean;
  public btnName: string;
  public itemsPerPage: number;
  public currentPage: number;
  public booksLength: number;
  public searchValue: string;
  public counter: any;

  constructor(private booksService: BooksService, public dialog: MatDialog) {
    this.searchValue = '';
    this.itemsPerPage = 8;
    this.currentPage = 1;
    this.showPopup = false;
  }

  ngOnInit() {
    this.loadBooks();
  }

  public loadBooks(): void {
    this.booksService.getBooks(this.itemsPerPage, this.currentPage).subscribe(
      (data: {bookslength: [{length: number}], books: IBookModel[]}) => {
        this.books = data.books as IBookModel[];
        if (data.bookslength.length < 1) {
          return;
        }
        this.booksLength = data.bookslength[0].length;
      });
  }

  public loadBooksBySearch(event): void {
    if (this.counter) {
      clearTimeout(this.counter);
    }
    this.counter = setTimeout(() => {
      this.booksService.getBooks(this.itemsPerPage, this.currentPage, event.target.value).subscribe(
        (data: any) => {
          this.booksLength = 0;
          this.books = data.books as IBookModel[];
          if (data.bookslength.length < 1) {
            return;
          }
          this.booksLength = data.bookslength[0].length;
        }
      );
    }, 500);
  }

  public addBook(): void {
    this.book = null;
    this.showPopup = true;
    this.btnName = 'Add';
  }

  public editBook(book: IBookModel): void {
    this.book = book;
    this.showPopup = true;
    this.btnName = 'Edit';
  }

  public deleteBook(id: string, title: string): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
      type: 'book',
      value: title,
    };
    const dialogRef = this.dialog.open(DeletePopupComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.booksService.deleteBook(id).subscribe(data => {
          this.loadBooks();
        });
      }
    });
  }

  public closePopup(event: boolean): void {
    this.showPopup = event;
    this.loadBooks();
  }

}
