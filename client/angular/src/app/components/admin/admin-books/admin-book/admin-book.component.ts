import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BooksService } from 'src/app/shared/services/books.service';
import { IBookModel } from '../../../../../../../../shared/models/book.model';


@Component({
  selector: 'admin-book',
  templateUrl: './admin-book.component.html',
  styleUrls: ['./admin-book.component.scss']
})
export class AdminBookComponent implements OnInit {
  public idBook: string;
  public book: IBookModel;

  constructor(private route: ActivatedRoute, private booksService: BooksService) {}

  ngOnInit() {
    this.getPageId();
    this.getBook();
    if (!this.book) {
      this.book = {
        _id: '',
        img: '',
        title: '',
        authors: []
      };
    }
  }

  public getPageId(): void {
    this.route.params.subscribe(params => {
      this.idBook = params['id'];
    });
  }

  public getBook(): void {
    this.booksService.getBook(this.idBook).subscribe( (data: IBookModel[]) => {
      data.map( item => this.book = item);
    });
  }

}
