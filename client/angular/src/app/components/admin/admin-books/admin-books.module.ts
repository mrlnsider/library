import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminBooksRoutingModule } from './admin-books-routing.module';
import { AdminBooksComponent } from './admin-books.component';
import { AdminBookComponent } from './admin-book/admin-book.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BookPopupComponent } from '../popup/book-popup/book-popup.component';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
    declarations: [
        AdminBooksComponent,
        AdminBookComponent,
        BookPopupComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        AdminBooksRoutingModule,
        ReactiveFormsModule,
        NgxPaginationModule
    ]
})
export class AdminBooksModule {}
