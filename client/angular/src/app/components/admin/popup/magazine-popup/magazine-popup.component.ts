import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { IMagazineModel } from '../../../../../../../../shared/models/magazine.model';
import { MagazineService } from 'src/app/shared/services/magazine.service';

@Component({
  selector: 'magazine-popup',
  templateUrl: './magazine-popup.component.html',
  styleUrls: ['./magazine-popup.component.scss']
})
export class MagazinePopupComponent implements OnInit {
  @Output() closePopupEvt = new EventEmitter();

  @Input() magazine: IMagazineModel;
  @Input() showPopup: boolean;
  @Input() btnName: string;

  public success: boolean;
  public imgBase: string;
  public myForm: FormGroup = new FormGroup({
    img: new FormControl(),
    title: new FormControl(),
    publisher: new FormControl()
  });

  constructor(private magazineService: MagazineService) {
    this.success = false;
  }

  ngOnInit() {
    if (!this.magazine) {
      this.magazine = {
        _id: '',
        img: '',
        title: '',
        publisher: ''
      };
    }
  }

  public chooseImg(event): void {
    const file = event.target.files[0];
    const saveImg = (img: string) => {
      this.imgBase = img;
    };
    const reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString());
    };
    reader.readAsDataURL(file);
  }

  public submit(id: string): void {
    this.success = true;
    const body: IMagazineModel = {
      _id: id || null,
      title: this.myForm.value.title,
      publisher: this.myForm.value.publisher,
      img: this.imgBase,
    };
    if (this.btnName === 'Edit') {
      this.magazineService.updateMagazine(body, id).subscribe((book: IMagazineModel) => {
        setTimeout(() => { this.closePopup(); }, 3000);
      });
      return;
    }
    this.magazineService.addMagazine(body).subscribe( () => {
      setTimeout(() => { this.closePopup(); }, 3000);
    });
  }

  public closePopup(): void {
    this.closePopupEvt.emit(false);
  }

}
