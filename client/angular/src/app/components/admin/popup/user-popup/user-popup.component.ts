import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { IUserModel } from '../../../../../../../../shared/models/user.model';
import { AdminService } from 'src/app/shared/services/admin.service';

@Component({
  selector: 'user-popup',
  templateUrl: './user-popup.component.html',
  styleUrls: ['./user-popup.component.scss']
})
export class UserPopupComponent implements OnInit {
  @Output() closePopupEvt = new EventEmitter();

  @Input() user: IUserModel;
  @Input() showPopup: boolean;
  @Input() btnName: string;

  public success = false;
  public myForm: FormGroup;

  constructor( private adminService: AdminService ) {
      this.myForm = new FormGroup({
          'email': new FormControl(),
          'password': new FormControl(),
          'fullName': new FormControl(),
          'role': new FormControl(),
          'confirm': new FormControl ()
      });
  }

  ngOnInit() {
    if (!this.user) {
      this.user = {
        _id: '',
        fullName: '',
        confirm: true,
        email: '',
        password: '',
        role: 1
      };
    }
  }

  public submit(id: string): void {
    this.success = true;
    setTimeout(() => { this.closePopup(); }, 3000);
    this.myForm.value.role = +this.myForm.value.role;
    if (this.btnName === 'Edit') {
      this.adminService.updateUser(this.myForm.value, id).subscribe( () => {});
      return;
    }
    this.adminService.addUser(this.myForm.value).subscribe( () => {});
  }

  public closePopup(): void {
    this.closePopupEvt.emit(false);
  }

}
