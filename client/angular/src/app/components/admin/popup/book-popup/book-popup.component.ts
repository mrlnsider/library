import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { IMagazineModel } from '../../../../../../../../shared/models/magazine.model';
import { IAuthorModel } from '../../../../../../../../shared/models/author.model';
import { IBookModel } from '../../../../../../../../shared/models/book.model';
import { BooksService } from 'src/app/shared/services/books.service';
import { AuthorService } from 'src/app/shared/services/author.service';
import { MagazineService } from 'src/app/shared/services/magazine.service';

@Component({
  selector: 'book-popup',
  templateUrl: './book-popup.component.html',
  styleUrls: ['./book-popup.component.scss']
})
export class BookPopupComponent implements OnInit {
  @Output() closePopupEvt = new EventEmitter();

  @Input() book: IBookModel;
  @Input() showPopup: boolean;
  @Input() btnName: string;

  public authors: IAuthorModel[];
  public isMagazine: string;
  public magazines: IMagazineModel[];
  public success: boolean;
  public chooseAuthors = [];
  public err: string;
  public imgBase: string;
  public myForm: FormGroup = new FormGroup({
    img: new FormControl(),
    title: new FormControl(),
    magazine: new FormControl(),
    authors: new FormControl()
  });

  constructor(private booksService: BooksService, private authorService: AuthorService, private magazineService: MagazineService) {
    this.success = false;
  }

  ngOnInit() {
    this.loadAuthors();
    this.loadMagazines();
    if (!this.book) {
      this.book = {
        _id: '',
        img: '',
        title: '',
        magazine: '',
        authors: []
      };
    }
    this.book.authors.forEach(authorId => {
      this.chooseAuthors.push(authorId);
    });
  }

  public loadAuthors(): void {
    this.authorService.getAuthors().subscribe(
      data => {
        this.authors = data as IAuthorModel[];
      }
    );
  }

  public loadMagazines(): void {
    this.magazineService.getMagazines().subscribe(
      data => {
        this.magazines = data as IMagazineModel[];
        this.magazineStatus();
      }
    );
  }

  public magazineStatus(): void {
    if (this.book.magazine[0] === undefined) {
        this.isMagazine = 'false';
      return;
    }
    this.magazines.map(item => {
      if (item.title === this.book.magazine[0].title) {
        this.isMagazine = item.title;
      }
    });
  }

  public chooseImg(event): void {
    const file = event.target.files[0];
    const saveImg = (img: string) => {
      this.imgBase = img;
    };
    const reader = new FileReader();
    reader.onloadend = function () {
      saveImg(reader.result.toString());
    };
    reader.readAsDataURL(file);
  }

  public chooseAuthor(event): void {
    this.err = '';
    const bool = this.chooseAuthors.find(item => {
      return item._id === event.target.value;
    });
    if (bool === undefined) {
      this.authors.map(author => {
        if (author._id === event.target.value) {
          this.chooseAuthors.push(author);
        }
      });
      return;
    }
    this.err = 'Author has been already add';
  }

  public deleteAuthor(ind: number): void {
    this.err = '';
    this.chooseAuthors.splice(ind, 1);
  }

  public submit(id: string): void {
    this.success = true;
    setTimeout(() => { this.closePopup(); }, 3000);
    const body: IBookModel = {
      _id: id || null,
      title: this.myForm.value.title,
      authors: this.chooseAuthors,
      img: this.imgBase,
      magazine: (() => {
        if (this.myForm.value.magazine === '') {
          return null;
        }
        return this.myForm.value.magazine;
      })()
    };
    console.log(body);
    if (this.btnName === 'Edit') {
      this.booksService.updateBook(body, id).subscribe( () => {
      });
      return;
    }
    this.booksService.addBook(body).subscribe(() => {
    });
  }

  public closePopup(): void {
    this.closePopupEvt.emit(false);
  }

}
