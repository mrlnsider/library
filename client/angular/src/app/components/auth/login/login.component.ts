import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public error: string;
  public myForm: FormGroup;

  constructor( private authService: AuthService, private router: Router, public localStorageService: LocalStorageService ) {
    this.myForm = new FormGroup({
      'email': new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required,
      ])),
      'password' : new FormControl('', Validators.compose([
        Validators.required,
      ])),
    });
  }

  ngOnInit() {

  }

  public submit(): void {
    this.authService.login(this.myForm.value).subscribe(
      (data: any) => {
        this.localStorageService.setItem('token', data['token']);
        data.user.role === 1 ? this.router.navigate(['/user']) : this.router.navigate(['/admin']);
      },
      error => {
        this.error = error.error;
      }
    );
  }

}
