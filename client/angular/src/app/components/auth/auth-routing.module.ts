import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [{
    path: '',
    component: AuthComponent,
    children: [
        {
            path: '',
            component: MainComponent
        },
        {
            path: 'register',
            loadChildren: './register/register.module#RegisterModule'
        },
        {
            path: 'login',
            loadChildren: './login/login.module#LoginModule',
        }
    ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {}
