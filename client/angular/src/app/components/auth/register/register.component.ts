import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../shared/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public success: boolean;
  public registr: boolean;
  public errText: string;
  public myForm: FormGroup;

  constructor(private authService: AuthService, private router: Router) {
    this.registr = false;
    this.myForm = new FormGroup({
      'email': new FormControl('', [
        Validators.required,
        Validators.pattern('[a-zA-Z0-9_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}')
      ]),
      'password': new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9_-]+$')
      ])),
      'fullName': new FormControl('', [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(50),
        Validators.pattern('^[a-zA-Z]+$')]),
      'confirm': new FormControl(false)
    });
  }

  ngOnInit() {

  }

  public submit(): void {
    this.authService.register(this.myForm.value).subscribe(
      () => {
        this.registr = true;
        this.success = true;
        setTimeout(() => this.router.navigate(['/auth/login']), 3000);
    },
      err => {
        this.registr = true;
        this.success = false;
        this.errText = err.error;
        setTimeout(() => this.registr = false, 3000);
      }
    );
  }

  public googleAuth(): void {
    this.authService.googleAuth().subscribe( () => {},
      error => console.log(error)
    );
  }

}
