import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'popup-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  @Output() closePopupEvt = new EventEmitter();

  @Input() user;
  @Input() showPopup: boolean;
  @Input() btnName: string;

  public success = false;
  public myForm: FormGroup;

  constructor( private userService: UserService ) {
      this.myForm = new FormGroup({
          'email': new FormControl(),
          'fullName': new FormControl()
      });
  }

  ngOnInit() {
  }

  public submit(id: string): void {
    this.success = true;
    setTimeout(() => { this.closePopup(); }, 3000);
    const body = {
      email: this.myForm.value.email || this.user.email,
      fullName: this.myForm.value.fullName || this.user.fullName
    };
    this.userService.updateUser(body, id).subscribe( () => {
    });
  }

  public closePopup(): void {
    this.closePopupEvt.emit(false);
  }

}
