import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { IUserModel } from '../../../../../../../shared/models/user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  public user: { fullName: string, email: string };
  public showPopup: boolean;
  public togglePassword: boolean;
  public passwordForm: FormGroup;
  public showText: boolean;
  public dataText: string;
  public showError: boolean;
  public errorText: string;

  constructor(private userService: UserService ) {
    this.togglePassword = false;
    this.showText = false;
    this.passwordForm = new FormGroup({
      'oldPassword': new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ])),
      'newPassword': new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      ]))
    });
  }

  ngOnInit() {
    if (!this.user) {
      this.user = {
        fullName: '',
        email: ''
      };
    }
    this.getUserInfo();
  }

  public getUserInfo(): void {
    this.userService.getUserInfo().subscribe(
      (data: any) => {
          this.userService.getUserById(data.id).subscribe(
            (user: IUserModel) => {
              this.user = user;
            }
          );
        }
    );
  }

  public editUser(user): void {
    this.user = user;
    this.showPopup = true;
  }

  public showPassword(): void {
    this.togglePassword = !this.togglePassword;
  }

  public editPass(id: string): void {
    this.userService.updatePass(this.passwordForm.value.oldPassword, this.passwordForm.value.newPassword, id).subscribe(
      () => {
        this.showError = false;
        this.dataText = 'Password changed';
        this.showText = true;
        this.togglePassword = false;
        setTimeout(() => {
          this.showError = false;
        }, 3000);
      },
      err => {
        this.errorText = err.error;
        this.showError = true;
      }
    );
  }

  public closePopup(event: boolean): void {
    this.showPopup = event;
    this.getUserInfo();
  }
}
