import { NgModule } from '@angular/core';
import { UserComponent } from './user.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserHeaderComponent } from '../../containers/user-header/user-header.component';
import { NavComponent } from '../../containers/user-header/nav/nav.component';
import { EditUserComponent } from './popup/edit-user/edit-user.component';
import { UserAuthorComponent } from './user-author/user-author.component';


@NgModule({
  declarations: [
    UserComponent,
    UserHeaderComponent,
    NavComponent,
    UserProfileComponent,
    EditUserComponent,
    UserAuthorComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule
  ]
})
export class UserModule {}
