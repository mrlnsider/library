import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserAuthorComponent } from './user-author/user-author.component';


const routes: Routes = [{
  path: '',
  component: UserComponent,
  children: [
    {
      path: '',
      component: UserProfileComponent
    },
    {
      path: 'books',
      loadChildren: './user-books/user-books.module#BooksModule',
    },
    {
      path: 'author/:id',
      component: UserAuthorComponent
    },
    { path: '**', redirectTo: '' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
