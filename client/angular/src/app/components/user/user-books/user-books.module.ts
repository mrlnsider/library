import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BooksComponent } from './user-books.component';
import { BooksRoutingModule } from './user-books-routing.module';
import { BookComponent } from './book/book.component';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
    declarations: [
        BooksComponent,
        BookComponent
    ],
    imports: [CommonModule,
        ReactiveFormsModule,
        BooksRoutingModule,
        NgxPaginationModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class BooksModule {}
