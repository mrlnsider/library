import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IBookModel } from '../../../../../../../../shared/models/book.model';
import { BooksService } from 'src/app/shared/services/books.service';

@Component({
  selector: 'user-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  public idBook: string;
  public book: IBookModel;

  constructor(private route: ActivatedRoute, private booksService: BooksService) {}

  ngOnInit() {
    this.getPageId();
    this.getBook();
    if (!this.book) {
      this.book = {
        _id: '',
        img: '',
        title: '',
        authors: []
      };
    }
  }

  public getPageId(): void {
    this.route.params.subscribe(params => {
      this.idBook = params['id'];
    });
  }

  public getBook(): void {
    this.booksService.getBook(this.idBook).subscribe( (data: IBookModel[]) => {
      data.map( item => this.book = item);
    });
  }

}
