import { Component, OnInit } from '@angular/core';
import { IBookModel } from '../../../../../../../shared/models/book.model';
import { BooksService } from 'src/app/shared/services/books.service';

@Component({
  selector: 'user-books',
  templateUrl: './user-books.component.html',
  styleUrls: ['./user-books.component.scss']
})
export class BooksComponent implements OnInit {

  public books: IBookModel[];
  public book: IBookModel;

  public itemsPerPage: number;
  public currentPage: number;
  public booksLength: number;
  public searchValue: string;
  public counter: any;

  constructor(private booksService: BooksService) {
    this.searchValue = '';
    this.itemsPerPage = 8;
    this.currentPage = 1;
  }

  ngOnInit() {
    this.loadBooks();
  }

  public loadBooks(): void {
    this.booksService.getBooks(this.itemsPerPage, this.currentPage).subscribe(
      (data: any) => {
        this.books = data.books as IBookModel[];
        if (data.bookslength.length < 1) {
          return;
        }
        this.booksLength = data.bookslength[0].length;
      });
  }

  public loadBooksBySearch(event): void {
    if (this.counter) {
      clearTimeout(this.counter);
    }
    this.counter = setTimeout(() => {
      this.booksService.getBooks(this.itemsPerPage, this.currentPage, event.target.value).subscribe(
        (data: {bookslength: [{length: number}], books: IBookModel[]}) => {
          this.booksLength = 0;
          this.books = data.books as IBookModel[];
          if (data.bookslength.length < 1) {
            return;
          }
          this.booksLength = data.bookslength[0].length;
        }
      );
    }, 500);
  }

}
