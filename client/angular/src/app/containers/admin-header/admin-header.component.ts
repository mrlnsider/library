import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {
  public title = 'logo';
  public logoUrl = 'https://upload.wikimedia.org/wikipedia/ru/thumb/5/50/Queens_Library_logo.svg/1280px-Queens_Library_logo.svg.png';

  constructor(public localStorageService: LocalStorageService) { }

  ngOnInit() {
  }

  public logOut(): void {
    this.localStorageService.removeItem('token');
  }

}
