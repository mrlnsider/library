import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardsService } from './shared/guards/auth-guards.service';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: './components/auth/auth.module#AuthModule'
  },
  {
    path: 'user',
    canActivate: [AuthGuardsService],
    data: {role: 'User'},
    loadChildren: './components/user/user.module#UserModule',
  },
  {
    path: 'admin',
    canActivate: [AuthGuardsService],
    data: {role: 'Admin'},
    loadChildren: './components/admin/admin.module#AdminModule',
  },
  { path: '**', redirectTo: 'auth' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
