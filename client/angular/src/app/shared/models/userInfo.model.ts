export interface IUserInfoModel {
    confirm: boolean;
    email: string;
    exp: number;
    fullName: string;
    iat: number;
    id: string;
    role: number;
}
