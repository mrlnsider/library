import { Injectable } from '@angular/core';
import { IBookModel } from '../../../../../../shared/models/book.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class BooksService {

  constructor(private http: HttpClient) { }

  public getBooks(itemsPerPage: number, currentPage: number, bookName: string = ''): Observable<Object> {
    const body = {
      itemsPerPage: itemsPerPage,
      currentPage: currentPage,
      bookName: bookName
    };
    return this.http.post(environment.url + '/book', body);
  }

  public getBook(id: string): Observable<Object> {
    return this.http.get(environment.url + '/book/' + id);
  }

  public getBookByAuthor(id: string): Observable<Object> {
    return this.http.get(environment.url + '/book/get/' + id);
  }

  public addBook(book: IBookModel): Observable<Object> {
    return this.http.post(environment.url + '/book/add', book);
  }

  public updateBook(book: IBookModel, id: string): Observable<Object> {
    return this.http.put(environment.url + '/book/' + id, book);
  }

  public deleteBook(id: string): Observable<Object> {
    return this.http.delete(environment.url + '/book/' + id);
  }
}
