import { Injectable } from '@angular/core';
import { IUserModel } from '../../../../../../shared/models/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthService {

    constructor(private http: HttpClient) {

    }

    public register(user: IUserModel): Observable<Object> {
        const body = { fullName: user.fullName, email: user.email, password: user.password, confirm: user.confirm };
        return this.http.post(environment.url + '/auth/register', body);
    }

    public login(user: IUserModel): Observable<Object> {
        const body = { login: user.email, password: user.password };
        return this.http.post(environment.url + '/auth/login', body);
    }

    public googleAuth(): Observable<Object> {
        return this.http.get(environment.url + '/auth/google');
    }

}
