import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public getUserInfo(): Observable<Object> {
    return this.http.get(environment.url + '/auth/profile');
  }

  public getUserById(id: string): Observable<Object> {
    return this.http.get(environment.url + '/user/' + id);
  }

  public updateUser(user, id: string): Observable<Object> {
    return this.http.put(environment.url + '/user/' + id, user);
  }
  public updatePass(oldPass, newPass, id: string): Observable<Object> {
    const body = {
      oldPass: oldPass,
      newPass: newPass
    };
    return this.http.put(environment.url + '/user/pass/' + id, body);
  }
}
