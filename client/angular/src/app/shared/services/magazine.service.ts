import { Injectable } from '@angular/core';
import { IMagazineModel } from '../../../../../../shared/models/magazine.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class MagazineService {

  constructor(private http: HttpClient) {}

  public getMagazines(): Observable<Object> {
    return this.http.get(environment.url + '/magazine');
  }

  public getMagazine(id: string): Observable<Object> {
    return this.http.get(environment.url + '/magazine/' + id);
  }

  public addMagazine(magazine: IMagazineModel): Observable<Object> {
    return this.http.post(environment.url + '/magazine', magazine);
  }

  public updateMagazine(magazine: IMagazineModel, id: string): Observable<Object> {
    return this.http.put(environment.url + '/magazine/' + id, magazine);
  }

  public deleteMagazine(id: string): Observable<Object> {
    return this.http.delete(environment.url + '/magazine/' + id);
  }
}
