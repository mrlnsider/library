import { Injectable } from '@angular/core';
import { IAuthorModel } from '../../../../../../shared/models/author.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthorService {

  constructor(private http: HttpClient) {}

  public getAuthors(): Observable<Object> {
    return this.http.get(environment.url + '/author');
  }

  public getAuthor(id: string): Observable<Object> {
    return this.http.get(environment.url + '/author/' + id);
  }

  public addAuthor(author: IAuthorModel): Observable<Object> {
    const body = {
      name: author.name
    };
    return this.http.post(environment.url + '/author', body);
  }

  public updateAuthor(author: IAuthorModel): Observable<Object> {
    const body = {
      _id: author._id,
      name: author.name
    };
    return this.http.put(environment.url + '/author/' + author._id, body);
  }

  public deleteAuthor(id: string): Observable<Object> {
    return this.http.delete(environment.url + '/author/' + id);
  }
}
