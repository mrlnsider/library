import { Injectable } from '@angular/core';
import { IUserModel } from '../../../../../../shared/models/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class AdminService {

  constructor(private http: HttpClient) {}

  public getConfirmUsers(): Observable<Object> {
    return this.http.get(environment.url + '/admin/users');
  }

  public getNoConfirmUsers(): Observable<Object> {
    return this.http.get(environment.url + '/admin/confirm');
  }

  public addUser(user: IUserModel): Observable<Object> {
    const body = {
      confirm: user.confirm,
      email: user.email,
      fullName: user.fullName,
      password: user.password,
      role: user.role
    };
    return this.http.post(environment.url + '/admin/users', body);
  }

  public updateUser(user: IUserModel, id: string): Observable<Object> {
    const body = {
      confirm: user.confirm,
      email: user.email,
      fullName: user.fullName,
      password: user.password,
      role: user.role
    };
    return this.http.put(environment.url + '/admin/users/' + id, body);
  }

  public updateConfirmUser(id: string): Observable<Object> {
    const body = { confirm: true };
    return this.http.put(environment.url + '/admin/users/' + id, body);
  }

  public deleteUser(id: string): Observable<Object> {
    return this.http.delete(environment.url + '/admin/users/' + id);
  }

}


