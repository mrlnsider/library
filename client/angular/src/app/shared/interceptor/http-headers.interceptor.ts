import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders } from '@angular/common/http';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable()
export class HeadersInterceptor implements HttpInterceptor {

    constructor( public localStorageService: LocalStorageService ) {}


    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (!(req.url.indexOf('/auth/login') === -1)) {
            return next.handle(req); // do nothing
        }
        if (!(req.url.indexOf('/auth/register') === -1)) {
            return next.handle(req); // do nothing
        }
        if (!(req.url.indexOf('/auth/google') === -1)) {
            return next.handle(req); // do nothing
        }
        const paramReq = req.clone({
            headers: new HttpHeaders ({'x-access-token' : this.localStorageService.getItem('token')})
        });
        return next.handle(paramReq);
    }
}
