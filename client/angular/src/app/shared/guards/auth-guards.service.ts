import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable()
export class AuthGuardsService implements CanActivate {
  // public userRole: number;

  constructor(public router: Router, public localStorageService: LocalStorageService) {}

  canActivate(): boolean {
    if (this.isAuthenticated()) {
      return true;
    }
    return false;
  }

  public isAuthenticated(): boolean {
    return this.localStorageService.getItem('token') ? true : false;
  }
}

