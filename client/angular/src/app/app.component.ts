import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './shared/services/user.service';
import { IUserModel } from '../../../../shared/models/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit {

  constructor( private userService: UserService, private router: Router ) {

  }

  ngOnInit() {
    this.loadUserInfo();
  }

  public loadUserInfo(): void {
    this.userService.getUserInfo().subscribe(
      (data: IUserModel) => {
        if (data.role === 1) {
          this.router.navigate(['/user']);
        }
        if (data.role === 2) {
          this.router.navigate(['/admin']);
        }
      },
      error => {
        console.log('Please, log in');
      }
    );
  }

}
