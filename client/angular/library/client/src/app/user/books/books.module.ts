import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BooksComponent } from './books.component';
import { BooksRoutingModule } from './books-routing.module';
import { ItemComponent } from './item/item.component';


@NgModule({
    declarations: [
        BooksComponent,
        ItemComponent
    ],
    imports: [CommonModule,
        ReactiveFormsModule,
        BooksRoutingModule,
    ]
})
export class BooksModule {}
