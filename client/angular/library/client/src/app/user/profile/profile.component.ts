import { Component, OnInit } from '@angular/core';
import { LibraryService } from 'src/app/shared/services/library.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  private userToken = JSON.parse(localStorage.getItem('user'))['token'];
  userInfo = [];

  constructor(private service: LibraryService, ) { }

  ngOnInit() {
    this.service.getUserInfo(this.userToken).subscribe(
      data => this.userInfo.push(data)
    );
  }

}
