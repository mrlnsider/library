import { Component, OnInit } from '@angular/core';
import { LibraryService } from 'src/app/shared/services/library.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  users: any;
  noUser: Boolean = false;
  amountUsers: number;

  constructor( private service: LibraryService) { }

  loadUsers () {
    this.service.getConfirmUsers().subscribe (data => {
      console.log(data);
      this.users = data;
      this.amountUsers = this.users.length;
      if (this.users.length < 1) {
        this.noUser = true;
      }
    });
  }

  ngOnInit() {
    this.loadUsers();
  }

  deleteUser (id) {
    this.service.deleteUser(id).subscribe( data => {
      console.log(data);
      this.loadUsers();
    });
  }

  confirmUser (id) {
    this.service.updateConfirmUser(id).subscribe ( data => {
      console.log(data);
      this.loadUsers();
    });
  }

}
