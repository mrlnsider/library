import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.modules';
import { AdminComponent } from './admin.component';
import { HeaderComponent } from './header/header.component';
import { ConfirmComponent } from './confirm/confirm.component';

@NgModule({
    declarations: [
        AdminComponent,
        HeaderComponent,
        ConfirmComponent
    ],
    imports: [
        CommonModule,
        AdminRoutingModule
    ]
})
export class AdminModule {}
