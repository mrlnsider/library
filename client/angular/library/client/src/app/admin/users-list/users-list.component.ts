import { Component, OnInit } from '@angular/core';
import { LibraryService } from 'src/app/shared/services/library.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {
  users: any;
  showPopup = false;
  user: Object;
  btnName: String;
  status: String;

  constructor( private service: LibraryService) {

  }

  loadUsers () {
    this.service.getAllUsers().subscribe(
      data => {
        console.log(data);
        this.users = data;
      }
    );
  }

  ngOnInit() {
    this.loadUsers();
  }

  deleteUser (id) {
    this.service.deleteUser(id).subscribe( data => {
      console.log(data);
      this.loadUsers();
    });
  }

  editUser (user) {
    this.user = user;
    this.showPopup = true;
    this.btnName = 'Edit';
  }

  addUser () {
    this.user = null;
    this.showPopup = true;
    this.btnName = 'Add';

  }

  closePopup (event) {
    this.showPopup = event;
    this.loadUsers();
  }

}
