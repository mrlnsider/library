import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { UserListRoutingModule } from './users-list-routing.module';
import { UsersListComponent } from './users-list.component';
import { PopupComponent } from './popup/popup.component';

@NgModule({
    declarations: [
        UsersListComponent,
        PopupComponent
    ],
    imports: [
        CommonModule,
        UserListRoutingModule,
        ReactiveFormsModule
    ]
})
export class UserListModule {}
