import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LibraryService } from 'src/app/shared/services/library.service';
import { IUserModel } from 'src/app/shared/models/library.models';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {

  @Output() closePopupEvt = new EventEmitter();

  @Input() user: IUserModel;
  @Input() showPopup: boolean;
  @Input() btnName: string;

  myForm: FormGroup;
  constructor( private service: LibraryService ) {
      this.myForm = new FormGroup({
          'email': new FormControl(),
          'password': new FormControl(),
          'fullName': new FormControl(),
          'role': new FormControl(),
          'confirm': new FormControl ()
      });
  }

  ngOnInit() {
    if (!this.user) {
      this.user = {
        fullName: '',
        confirm: true,
        email: '',
        password: '',
        role: 1
      };
    }
  }

  closePopup () {
    this.closePopupEvt.emit(false);
  }

  submit(id) {
    this.myForm.value.role = +this.myForm.value.role;
    console.log(this.myForm.value, id);
    this.service.updateUser(this.myForm, id).subscribe( data => {
      console.log(data);
    });
  }

}
