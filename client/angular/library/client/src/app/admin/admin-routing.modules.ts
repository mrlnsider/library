import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ConfirmComponent } from './confirm/confirm.component';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  children: [
    {
      path: '',
      component: ConfirmComponent
    },
    {
      path: 'users',
      loadChildren: './users-list/users-list.module#UserListModule',
    },
    {
      path: 'books',
      loadChildren: './admin-books/admin-books.module#AdminBooksModule',
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
