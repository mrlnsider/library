import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminBooksRoutingModule } from './admin-books-routing.module';
import { AdminBooksComponent } from './admin-books.component';

@NgModule({
    declarations: [
        AdminBooksComponent
    ],
    imports: [
        CommonModule,
        AdminBooksRoutingModule
    ]
})
export class AdminBooksModule {}
