import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public title = 'logo';
  public logoUrl = 'https://upload.wikimedia.org/wikipedia/ru/thumb/5/50/Queens_Library_logo.svg/1280px-Queens_Library_logo.svg.png';

  constructor() { }

  ngOnInit() {
  }

  public logOut () {
    localStorage.removeItem('user');
  }
}
