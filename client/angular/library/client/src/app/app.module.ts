import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './containers/main/main.component';
import { LibraryService } from './shared/services/library.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuardsService } from './shared/guards/auth-guards.service';
import { LocalStorageService } from './shared/services/local-storage.service';
import { AdminGuardsService } from './shared/guards/admin-guards.service';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    LibraryService,
    AuthGuardsService,
    AdminGuardsService,
    LocalStorageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
