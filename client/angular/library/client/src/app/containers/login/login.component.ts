import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LibraryService } from '../../shared/services/library.service';
import { Router } from '@angular/router';
import { LocalStorageService } from 'src/app/shared/services/local-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public error: String;


myForm: FormGroup;
constructor( private servise: LibraryService, private router: Router ) {
    this.myForm = new FormGroup({
        'email': new FormControl('', [
                            Validators.required,
                            Validators.pattern('[a-zA-Z_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}')
                        ]),
        'password' : new FormControl('', Validators.compose([
          Validators.minLength(6),
          Validators.required,
          Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
        ])),
    });
}

submit(event) {
  event.preventDefault();
  this.servise.postLog(this.myForm.value).subscribe(
    data => {
      console.log(data);
      localStorage.setItem('user', JSON.stringify(data)); // ??? need or no (use in Guard)
      const userInfo: any = data;
      userInfo.user.role === 1 ? this.router.navigate(['/user']) : this.router.navigate(['/admin']);
    },
    error => {
      if (error.status) {
        this.error = 'No user found';
      }
    }
  );

}

ngOnInit() {

}

}
