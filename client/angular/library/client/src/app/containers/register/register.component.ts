import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LibraryService } from '../../shared/services/library.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    public success: Boolean = false;


  myForm: FormGroup;
  constructor( private servise: LibraryService, private router: Router ) {
      this.myForm = new FormGroup({
          'email': new FormControl('', [
                              Validators.required,
                              Validators.pattern('[a-zA-Z_]+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}')
                          ]),
          'password' : new FormControl('', Validators.compose([
            Validators.minLength(6),
            Validators.required,
            Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
          ])),
          'fullName': new FormControl('', [Validators.required, Validators.minLength(2),
             Validators.maxLength(150), Validators.pattern('^[a-zA-Z]+$')]),
          'confirm' : new FormControl (false)
      });
  }

  submit(event) {
    event.preventDefault();
    this.servise.postReg(this.myForm.value).subscribe(data => {
        console.log(data);
    });
    this.success = true;
    setTimeout(() => this.router.navigate(['/auth/login']), 3000);
  }

  ngOnInit() {

  }
}
