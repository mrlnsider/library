import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './containers/main/main.component';
import { AuthGuardsService } from './shared/guards/auth-guards.service';
import { AdminGuardsService } from './shared/guards/admin-guards.service';

const routes: Routes = [
  { path: '', component: MainComponent },
  {
    path: 'auth/register',
    loadChildren: './containers/register/register.module#RegisterModule'
  },
  {
    path: 'auth/login',
    loadChildren: './containers/login/login.module#LoginModule',
  },
  {
    path: 'user',
    canActivate: [AuthGuardsService],
    loadChildren: './user/user.module#UserModule',
  },
  {
    path: 'admin',
    canActivate: [AdminGuardsService],
    loadChildren: './admin/admin.module#AdminModule',
  },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
