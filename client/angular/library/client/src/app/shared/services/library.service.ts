import { Injectable } from '@angular/core';
import { IUserModel } from '../models/library.models';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class LibraryService {
    // private token = JSON.parse(localStorage.getItem('user'))['token'];

    constructor(private http: HttpClient) { }

    getToken () {
        return JSON.parse(localStorage.getItem('user'))['token'];
    }

    postReg(user: IUserModel) {
        const body = { fullName: user.fullName, email: user.email, password: user.password, confirm: user.confirm};
        return this.http.post('https://localhost:3000/auth/register', body);
    }
    postLog(user: IUserModel) {
        const body = { login: user.email, password: user.password };
        return this.http.post('https://localhost:3000/auth/login', body);
    }
    getUserInfo(token: string) {
        return this.http.get('https://localhost:3000/auth/profile', {headers: {'x-access-token' : token}});
    }
    getAllUsers() {
        return this.http.get('https://localhost:3000/admin/users', {headers: {'x-access-token' : this.getToken()}});
    }
    addUser(user) {
        const body = { fullName: user.fullName, email: user.email, password: user.password, role: user.role };
        return this.http.post('https://localhost:3000/auth/register', body);
    }
    updateUser(user, id: string) {
        const body = {
            confirm: user.value.confirm,
            email: user.value.email,
            fullName: user.value.fullName,
            password: user.value.password,
            role: user.value.role
        };
        return this.http.put('https://localhost:3000/admin/users/' + id, body , {headers: {'x-access-token' : this.getToken()}});
    }
    deleteUser(id: string) {
        return this.http.delete('https://localhost:3000/admin/users/' + id, {headers: {'x-access-token' : this.getToken()}});
    }

    getConfirmUsers () {
        return this.http.get('https://localhost:3000/admin/confirm', {headers: {'x-access-token' : this.getToken()}});
    }
    updateConfirmUser(id: string) {
        const body = { confirm: true };
        return this.http.put('https://localhost:3000/admin/users/' + id, body , {headers: {'x-access-token' : this.getToken()}});
    }
}
