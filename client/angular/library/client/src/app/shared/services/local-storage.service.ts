import { Injectable } from '@angular/core';

@Injectable()

export class LocalStorageService {
  constructor() { }

  getLocalStorage (key: string) {
    return JSON.parse(localStorage.getItem(key));
  }
}
