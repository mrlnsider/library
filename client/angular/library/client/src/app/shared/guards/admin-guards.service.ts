import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable()
export class AdminGuardsService implements CanActivate {

  constructor(public router: Router, public service: LocalStorageService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot ): boolean {
    if (this.isAuthenticated()) {
      return true;
    }
    if (this.isAuthenticated() === false) {
      return false;
    }
  }

  isAuthenticated() {
    const userInfo = this.service.getLocalStorage('user');
    return userInfo ? userInfo.token : false;
  }
}
