export interface IUserModel {
    fullName: string;
    email: string;
    password: string;
    confirm: boolean;
    role?: number;
}
