export interface IUserModel {
    _id: any;
    email: string,
    fullName: string,
    password: string,
    confirm: boolean,
    role: UserRole
}

export enum UserRole {
    user = 1,
    admin = 2
}