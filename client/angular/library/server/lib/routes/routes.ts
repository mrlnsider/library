import { Request, Response, NextFunction } from "express";
import { BookController } from "../controllers/bookController";
import { AuthorController } from "../controllers/authorController";
import { AuthMiddleware } from "../middlewares/authMiddleware";
import { AuthController } from "../controllers/authController"
import { UserRole } from "../../../shared/models";

export class Routes {

    public bookController: BookController = new BookController()
    public authorController: AuthorController = new AuthorController()
    public authController: AuthController = new AuthController()

    public routes(app): void {

        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'GET request successfulll!!!!'
                })
            })

        // Auth
        app.route('/auth/profile')
            .get(AuthMiddleware([UserRole.user]), this.authController.profile);
        app.route('/auth/register')
            .post(this.authController.register);
        app.route('/auth/login')
            .post(this.authController.login);

        // Admin
        app.route('/admin/users')
            .get(AuthMiddleware([UserRole.admin]), this.authController.getConfirmUsers)
            .post(AuthMiddleware([UserRole.admin]), this.authController.adminRegister)
        app.route('/admin/confirm')
            .get(AuthMiddleware([UserRole.admin]), this.authController.getNoConfirmUsers)
        app.route('/admin/confirm/:id')
            .put(AuthMiddleware([UserRole.admin]), this.authController.updateConfirmUser)
        app.route('/admin/users/:id')
            //.post(AuthMiddleware([UserRole.admin]), this.authController.adminRegister)
            .put(AuthMiddleware([UserRole.admin]), this.authController.updateUser)
            .delete(AuthMiddleware([UserRole.admin]), this.authController.deleteUser)

        // Book 
        app.route('/book')
            .post(AuthMiddleware([UserRole.user]), this.bookController.add);
        app.route('/book/get/:authorId')
            .get(AuthMiddleware([UserRole.user]), this.bookController.getByAuthorId)
        app.route('/book/get')
            .get(AuthMiddleware([UserRole.user]), this.bookController.get)
        app.route('/book/:bookId')
            .get(AuthMiddleware([UserRole.user]), this.bookController.getById)
            .put(AuthMiddleware([UserRole.user]), this.bookController.update)
            .delete(AuthMiddleware([UserRole.user]), this.bookController.delete)

        // Author 
        app.route('/author')
            .get(AuthMiddleware([UserRole.user]), this.authorController.get)
            .post(AuthMiddleware([UserRole.user]), this.authorController.add);
        app.route('/author/:authorId')
            .get(AuthMiddleware([UserRole.user]), this.authorController.getById)
            .put(AuthMiddleware([UserRole.user]), this.authorController.update)
            .delete(AuthMiddleware([UserRole.user]), this.authorController.delete)

    }
}