
import { RequestModel, RequestPost } from '../middlewares/authMiddleware';
import { Response } from 'express';
import * as bcryptjs from "bcryptjs";
import * as jsonwebtoken from "jsonwebtoken";
import { authConfig } from "../config"
import { UserRepository } from '../repositories/userRepository';
import { UserRole } from '../../../shared/models';

export class AuthController {

    public register(req: RequestPost<{ fullName: string, email: string, password: string, confirm: boolean }>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
        console.log(req.body)
        UserRepository.create({
            fullName: req.body.fullName,
            email: req.body.email,
            password: hashedPassword,
            confirm: req.body.confirm,
            role: UserRole.user
        },
            (err, user) => {
                if (err) return res.status(500).send("There was a problem registering the user.")
                // create a token
                var token = jsonwebtoken.sign({ id: user._id, fullName: user.fullName, email: user.email, confirm: user.confirm, role: UserRole.user }, authConfig.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });
                res.status(200).send({ auth: true, token: token, user: user });
            });
    }

    public login(req: RequestPost<{ login: string, password: string }>, res: Response) {
        UserRepository.findOne({ email: req.body.login }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            if (!user) return res.status(400).send('No user found.');
            var passwordIsValid = bcryptjs.compareSync(req.body.password, user.password);
            if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });
            var token = jsonwebtoken.sign({ id: user._id, fullName: user.fullName, email: user.email, role: user.role }, authConfig.secret, {
                expiresIn: 86400 // expires in 24 hours
            });
            res.status(200).send({ auth: true, token: token, user: user });
        });
    }

    public profile(req: RequestModel<{}>, res: Response) {
        res.status(200).send(req.user);
    }

    public adminRegister(req: RequestPost<{ fullName: string, email: string, password: string, confirm: boolean ,role: number }>, res: Response) {
        var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
        UserRepository.create({
            fullName: req.body.fullName,
            email: req.body.email,
            password: hashedPassword,
            confirm: req.body.confirm,
            role: req.body.role
        },
            (err, user) => {
                if (err) return res.status(500).send("There was a problem registering the user.")
                // create a token
                var token = jsonwebtoken.sign({ id: user._id, fullName: user.fullName, email: user.email, role: UserRole.user }, authConfig.secret, {
                    expiresIn: 86400 // expires in 24 hours
                });
                res.status(200).send({ auth: true, token: token, user: user });
            });
    }
    
    public updateUser (req: RequestModel<{id: string}>, res: Response) {
        if (req.body.password) {
            var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
            req.body.password = hashedPassword;
        }
        UserRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
        console.log(req.body)  
    }

    public deleteUser (req: RequestModel<{id: string}>, res: Response) {           
        UserRepository.remove({ _id: req.params.id }, (err) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json({ message: 'Successfully deleted user!'});
        });
    }

    public getConfirmUsers(req:  RequestModel<{}>, res: Response) {
        UserRepository.find({confirm: true}, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        })
    }

    public getNoConfirmUsers (req:  RequestModel<{}>, res: Response) {
        UserRepository.find({confirm: false}, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        })
    }

    public updateConfirmUser (req: RequestModel<{id: string}>, res: Response) {           
        UserRepository.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, user) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(user);
        });
    }
}
