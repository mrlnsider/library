export const authorsReducer = (state = [], action: any) => {
    if (action.type === 'AUTHORS_GET') {
      return action.payload;
    }
    return state;
  }