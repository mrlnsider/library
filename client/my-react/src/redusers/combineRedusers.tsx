import { combineReducers } from "redux";
import { roleReducer } from "./roleReducer";
import { authReducer } from "./authReducer";
import { booksReducer } from "../components/admin/admin-books/admin-books";
import { usersReducer } from "../components/admin/confirm-users/confirm-users";
import { confirmUsersReducer } from "../components/admin/users-list/users-list";
import { bookReducer } from "../components/admin/admin-books/admin-book/admin-book";
import { authorsReducer } from "./authorReducer";
import { authorReducer, booksByAuthorReducer } from "../components/admin/author-list/author/author";
import { magazinesReducer } from "./magazineReducer";
import { userInfoReducer } from "../components/user/user-profile/user-profile";




const reducers = combineReducers({
    users: usersReducer,
    auth: authReducer,
    role: roleReducer,
    confirmUsers: confirmUsersReducer,
    books: booksReducer,
    authors: authorsReducer,
    magazines: magazinesReducer,
    book: bookReducer,
    author: authorReducer,
    booksByAuthor: booksByAuthorReducer,
    userInfo: userInfoReducer
});

export default reducers;