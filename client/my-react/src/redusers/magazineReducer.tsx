export const magazinesReducer = (state = [], action: any) => {
    if (action.type === 'MAGAZINES_GET') {
        return action.payload;
    }
    return state;
}