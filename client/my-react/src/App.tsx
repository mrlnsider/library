import React, { ReactNode } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import './App.scss';
import Auth from './components/auth';
import Register from './components/auth/register';
import Login from './components/auth/login';
import Admin from './components/admin';
import { UserService } from './shared/services/user.service';
import { bindActionCreators } from 'redux';
import { actionRole, actionAuth } from './actions/actions';
import { LocalStorageService } from './shared/services/localStorage.service';
import jwt_decode from 'jwt-decode';
import { IUserModel } from '../../../shared/models/user.model';
import User from './components/user';

export const NotFound = () =>
  <div className="results">
    <h2>404 page not found</h2>
    <p>We are sorry but the page you are looking for does not exist.</p>
  </div>

class App extends React.Component<any, any> {
  public userService = new UserService;
  public localStorageService = new LocalStorageService;

  constructor(props: any) {
    super(props);
  }

  public getAuthStatus() {
    if (this.props.auth == false && this.localStorageService.getToken()) {
      let role: IUserModel = jwt_decode(this.localStorageService.getToken());
      this.props.actionRole(role.role);
      this.props.actionAuth(true);
      return
    }
  }

  public loadRoute(): any {
    this.getAuthStatus();
    if (this.props.role == 0 && (this.localStorageService.getToken() === '')) {
      return  <Switch>
                <Route path="/auth" component={Auth} exact />
                <Route path="/auth/register" component={Register} />
                <Route path="/auth/login" component={Login} />
                <Redirect from='/' to="/auth" />
                <Route path="*" component={NotFound} exact />
              </Switch>
    }
    if (this.props.role === 1) {
      return  <Switch>
                <Route path="/user" component={User} />
                <Redirect from="/auth*" to="/user"  />
                <Redirect from="/admin*" to="/user"  />
                <Redirect from="/" to="/user" exact />
                <Route path="*" component={NotFound} exact />
              </Switch>
    }
    if (this.props.role === 2) {
      return  <Switch>
                <Route path="/admin" component={Admin} />
                <Redirect from="/auth*" to="/admin"  />
                <Redirect from="/user*" to="/admin"  />
                <Redirect from="/" to="/admin" exact />
                <Route to="*" component={NotFound} exact />
              </Switch>
    }
  }


  render() {
    this.getAuthStatus()
    return (
      <React.Fragment>
        {this.loadRoute()}
      </React.Fragment>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    role: state.role
  }
}
function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({ actionAuth: actionAuth, actionRole: actionRole }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

