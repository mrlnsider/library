export function actionAuth(auth: boolean) {
    return {
        type: 'AUTH_CHANGE',
        payload: auth
    }
}

export function actionRole(role: number) {
    return {
        type: 'ROLE_CHANGE',
        payload: role
    }
}

export function actionAuthors(authors: []) {
    return {
        type: 'AUTHORS_GET',
        payload: authors
    }
}

export function actionMagazines(magazines: []) {
    return {
      type: 'MAGAZINES_GET',
      payload: magazines
    }
  }
