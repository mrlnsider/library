import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import "bootstrap/scss/bootstrap.scss"
import App from './App';
import { Provider } from 'react-redux';
import { createStore } from "redux";
import { Router, Switch } from 'react-router';
import createHistory from "history/createBrowserHistory";
import reducers from './redusers/combineRedusers';

const store = createStore(reducers);

ReactDOM.render(
    <Provider store={store}>
        <Router history={createHistory()}>
            <Switch >
                <App />
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);
