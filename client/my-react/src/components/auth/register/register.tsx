import * as React from 'react';
import './register.scss'
import { Link } from 'react-router-dom';
import { AuthService } from '../../../shared/services/auth.service';

class Register extends React.Component<any, any>  {
  public authService = new AuthService();
  constructor(props: any) {
    super(props);

    this.state = {
      fields: {},
      errors: {}
    }
  }

  public handleValidation() {
    let fields = this.state.fields;
    let errors: any = {};
    let formIsValid = true;
    //Name
    if (!fields["name"]) {
      formIsValid = false;
      errors["name"] = "Cannot be empty";
    }
    if (typeof fields["name"] !== "undefined") {
      if (!fields["name"].match(/^[a-zA-Z]+$/)) {
        formIsValid = false;
        errors["name"] = "Only letters";
      }
    }
    //Email
    if (!fields["email"]) {
      formIsValid = false;
      errors["email"] = "Cannot be empty";
    }
    if (typeof fields["email"] !== "undefined") {
      let lastAtPos = fields["email"].lastIndexOf('@');
      let lastDotPos = fields["email"].lastIndexOf('.');
      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') == -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = "Email is not valid";
      }
    }
    //Password
    if (!fields["password"]) {
      formIsValid = false;
      errors["password"] = "Cannot be empty";
    }
    if (typeof fields["password"] !== "undefined") {
      if (!fields["password"].match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9_-]+$/)) {
        formIsValid = false;
        errors["password"] = "Password should contain at least one character, number and capital letter";
      }
    }
    this.setState({ errors: errors });
    return formIsValid;
  }

  public contactSubmit(e: any) {
    e.preventDefault();
    const user = { 
      fullName: this.state.fields.name, 
      email: this.state.fields.email, 
      password: this.state.fields.password, 
      confirm: false 
    };
    if (this.handleValidation()) {
      this.authService.register(user).then( () => {
        this.props.history.push('/auth/login')
      })
    }
  }

  public handleChange(field: string, e: any) {
    let fields = this.state.fields;
    fields[field] = e.target.value;
    this.setState({ fields: fields });
  }

  public render() {
    return (
      <section className="container">
        <div className="register">
          <form className="my-form" onSubmit= {this.contactSubmit.bind(this)}>
            <div className="form-box">
              <h3 className="title">Register</h3>
              <fieldset>
                <div className="form-group">
                  <input
                    className="form-control"
                    ref="name" 
                    type="text" 
                    placeholder="Full Name" 
                    onChange={this.handleChange.bind(this, "name")} 
                  />
                  <span><i className="fas fa-user"></i></span>
                  <p style={{color: "red"}}>
                    {this.state.errors["name"]}
                  </p>
                </div>
                <div className="form-group">
                  <input
                    className="form-control"
                    ref="email" 
                    type="text" 
                    placeholder="Email" 
                    onChange={this.handleChange.bind(this, "email")} 
                  />
                  <span><i className="fas fa-envelope"></i></span>
                  <p style={{color: "red"}}>
                    {this.state.errors["email"]}
                  </p>
                </div>
                <div className="form-group">
                  <input
                    className="form-control"
                    ref="password" 
                    type="password"
                    placeholder="Password"
                    minLength={6}
                    onChange={this.handleChange.bind(this, "password")} 
                  />
                  <span><i className="fas fa-unlock-alt"></i></span>
                  <p style={{color: "red"}}>
                    {this.state.errors["password"]}
                  </p>
                </div>
                <div className="form-group">
                  <button
                    type="submit"
                    className="btn btn-success"
                  >
                    Register
                  </button>
                </div>
              </fieldset>
              <div className="link-login">
                <Link to='/auth/login' className="btn-reg"><span >Log in</span></Link>
              </div>
            </div>
          </form>
        </div>
      </section>
    );
  }
}

export default Register;
