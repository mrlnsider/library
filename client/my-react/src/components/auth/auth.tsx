import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './auth.scss';

class Auth extends Component  {
  public render() {
    return (
      <section className="container">
        <div className="reg-container">
          <div className="reg-box">
            <Link to='/auth/login' className="btn-log">Log in</Link>
              <span>or</span>
            <Link to='/auth/register' className="btn-reg">Register</Link>
          </div>
        </div>    
      </section>
    );
  }
}

export default Auth;
  