import * as React from 'react';
import './login.scss';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { actionAuth, actionRole } from '../../../actions/actions';
import { AuthService } from '../../../shared/services/auth.service';

class Login extends React.Component<any, any>  {
  public authService = new AuthService();
  constructor(props: any) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  public handleInputChange(event: any) {
    const value = event.target.value;
    const name =  event.target.name;

    this.setState({
      [name]: value
    });
  }

  public handleSubmit (event: any) {
    event.preventDefault();
 
    const user  = {
      login: this.state.email,
      password: this.state.password
    };
 
    this.authService.login(user)
      .then( res => {
        localStorage.setItem('token', res.data.token)
        this.props.actionAuth(true);
        this.props.actionRole(res.data.user.role)
        res.data.user.role === 1 ? this.props.history.push('/user') : this.props.history.push('/admin')
      })
  }

  public render() {
    return (
      <section className="container">
        <div className="login">
          <form className="my-form" onSubmit={this.handleSubmit}>
            <div className="form-box">
              <h3 className="title">Log In</h3>
              <div className="form-group">
              <input
                type="email"
                placeholder="Email"
                name="email"
                onChange={this.handleInputChange}
              />
              <span><i className="fas fa-envelope"></i></span>
              </div>
              <div className="form-group">
                <input
                  type="password"
                  placeholder="Password"
                  name="password"
                  className="password"
                  onChange={this.handleInputChange}
                />
                <span><i className="fas fa-unlock-alt"></i></span>
              </div>
              <div className="err"></div>
              <div className="form-group">
                <button
                  type="submit"
                  className="btn-suc">
                  Log In
                </button>
              </div>
              <div className="link-reg">
                <Link to='/auth/register' className="btn-reg"><span >Register</span></Link>
              </div>
            </div> 
          </form>
        </div>
      </section>
    );
  }
}
function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    role: state.role
  }
}
function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({actionAuth: actionAuth, actionRole: actionRole}, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Login)
  