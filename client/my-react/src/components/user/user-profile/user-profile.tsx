import * as React from 'react';
import './user-profile.scss'
import ReactModal from 'react-modal';
import { UserService } from '../../../shared/services/user.service';
import { bindActionCreators, Dispatch, Action } from 'redux';
import { connect } from 'react-redux';
import { IUserModel } from '../../../../../../shared/models/user.model';

class UserProfile extends React.Component<any, any>  {
  public userService = new UserService()
  public user = {
    fullName: '',
    email: ''
  };
  public password = {
    oldPass: '',
    newPass: ''
  }
  constructor(props: any) {
    super(props);
    this.state = {
      success: false,
      modalIsOpen: false,
      togglePassword: false,
      btnName: '',
      err: '',
    };
    this.closeModal = this.closeModal.bind(this);
    this.editUser = this.editUser.bind(this);
  }

  componentWillMount() {
    this.getUserInfo();
  }
  public getUserInfo(): void {
    this.userService.getUserInfo().then(
      res => {
        this.userService.getUserById(res.data.id).then(
          resp => {
            this.props.actionUserInfo(resp.data)
          }
        );
      }
    );
  }

  public editUser(user: IUserModel): void {
    this.user = user;
    this.setState({
      modalIsOpen: true,
    })
  }

  public editPass(event: any, id: string): void {
    event.preventDefault();
    this.userService.updatePass(this.password.oldPass, this.password.newPass, id).then(() => {
      this.setState({
        togglePassword: !this.state.togglePassword
      })
    });
  }

  public submit(event: any, id: string): void {
    event.preventDefault();
    setTimeout(() => { this.closeModal(); }, 3000);
    const body = {
      email: this.user.email,
      fullName: this.user.fullName
    };
    this.userService.updateUser(body, id).then(() => {
      this.setState({
        success: true
      });
    });
  }

  public closeModal(): void {
    this.setState({
      modalIsOpen: false,
      success: false,
    });
    this.user = {
      fullName: '',
      email: ''
    }
    this.getUserInfo();
  }

  public render() {
    let showForm = { display: this.state.success ? 'none' : 'inline-block' };
    let showSuccess = { display: this.state.success ? 'inline-block' : 'none' };
    let showPassword = { display: this.state.togglePassword ? 'inline-block' : 'none' };
    return (
      <section className="container">
        <div className="user-form">
          <div className="item-box">
            <div className="icon">
              <i className="fas fa-user"></i>
            </div>
            <div>Your email: {this.props.userInfo.email}</div>
            <div>Your name: {this.props.userInfo.fullName}</div>
            <button className="btn-edit">
              <i onClick={() => this.editUser(this.props.userInfo)} className="fas fa-user-edit"></i>
            </button>
            <div className="password" onClick={() => this.setState({ togglePassword: !this.state.togglePassword })} >
              <p>Change password</p>
            </div>
            <div style={showPassword} >
              <form className="my-form" onSubmit={() => this.editPass(event, this.props.userInfo._id)}>
                <div className="form-group">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="Current Password"
                    onChange={(event) => this.password.oldPass = event.target.value}
                  />
                  <span><i className="fas fa-unlock-alt"></i></span>
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    className="form-control"
                    placeholder="New Password"
                    onChange={(event) => this.password.newPass = event.target.value}
                  />
                  <span><i className="fas fa-unlock-alt"></i></span>
                </div>
                <button className="btn btn-pass" >Edit</button>
              </form>
            </div>
          </div>
        </div>
        <ReactModal
          className="popup"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel="BookModal"
        >
          <div className="popup-box">
            <form className="form-book" onSubmit={() => this.submit(event, this.props.userInfo._id)} style={showForm}>
              <div className="form-box">
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Full Name"
                    defaultValue={this.user.fullName}
                    onChange={(event) => this.user.fullName = event.target.value}
                  />
                  <span><i className="fas fa-user"></i></span>
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Email"
                    value={this.user.email}
                    onChange={(event) => this.user.email = event.target.value}
                  />
                  <span><i className="fas fa-envelope"></i></span>
                </div>
                <div className="form-group">
                  <button
                    type="submit"
                    className="btn">
                    Edit
                    </button>
                </div>
              </div>
            </form>
            <div className="my-form" style={showSuccess}>
              <div className="form-box">
                <div className="success">User has been {this.state.btnName.toLowerCase()}<i className="fas fa-check-circle"></i></div>
              </div>
            </div>
            <button className="btn-close" onClick={this.closeModal}><i className="fas fa-times"></i></button>
          </div>
        </ReactModal>
      </section>
    );
  }
}

export const userInfoReducer = (state = [], action: any) => {
  if (action.type === 'USER_INFO_GET') {
    return action.payload;
  }
  return state;
}

function actionUserInfo(userInfo: []) {
  return {
    type: 'USER_INFO_GET',
    payload: userInfo
  }
}

function mapStateToProps(state: any) {
  return {
    userInfo: state.userInfo
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionUserInfo: actionUserInfo }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile)