import * as React from 'react';
import { Route, Switch } from 'react-router';
import { NotFound } from '../../App';
import UserBooks from './user-books'
import UserProfile from './user-profile';
import UserHeader from '../../containers/user-header'
import UserBook from './user-books/user-book';
import UserAuthor from './user-author';

class User extends React.Component<any, any>  {
  public render() {
    return (
      <React.Fragment>
        <UserHeader item = {this.props} />
        <main>
          <Switch>
            <Route path='/user' exact component={UserProfile} />
            <Route path='/user/books' component={UserBooks} exact/>
            <Route path='/user/books/:id' component={UserBook} />
            <Route path='/user/author/:id' component={UserAuthor} />
            <Route to="*" component={NotFound} exact />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default User;
  