import * as React from 'react';
import './user-book.scss'
import { BooksService } from '../../../../shared/services/books.service';
import { IBookModel } from '../../../../../../../shared/models/book.model';
import { IAuthorModel } from '../../../../../../../shared/models/author.model';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { IMagazineModel } from '../../../../../../../shared/models/magazine.model';
import { Link } from 'react-router-dom';

class UserBook extends React.Component<any, any>  {
  public booksService = new BooksService()
  constructor(props: any) {
    super(props)
  }
  componentWillMount() {
    this.loadBook();
  }

  public loadBook(): void {
    this.booksService.getBook(this.props.match.params.id).then((res: { data: [] }) => {
      res.data.map((book: IBookModel) => {
        this.props.actionBook(book)
      })
    })
  }

  public render() {
    return (
      <section className="container">
        <div className="item-book">
          <div className="item-box">
            <div className="item-img">
              <img src={this.props.book.img} />
            </div>
            <div className="item-descr">
              <div className="item-title">
                <h1>{this.props.book.title}</h1>
              </div>
              <div className="item-author">
                <p>by &nbsp;
                  {this.props.book.authors.map((author: IAuthorModel, index: number) => {
                  return <Link to={{ pathname: '/user/author/' + author._id }} key={index}>
                    <span className="item-author-name">{author.name}; </span>
                  </Link>
                })}
                </p>
              </div>
            </div>
          </div>
          <div >
            <div className="item-magazine" >
              {this.props.book.magazine.map((magazine: IMagazineModel, index: number) => {
                return <React.Fragment key={index}>
                  <span><i className="fas fa-plus"></i></span>
                  <div className="magazine-img">
                    <img src={magazine.img} />
                  </div>
                </React.Fragment>
              })}
            </div>
          </div>
        </div>
      </section >
    );
  }
}

const book = {
  authors: [],
  magazine: []
}

export const bookReducer = (state = book, action: any) => {
  if (action.type === 'BOOK_GET') {
    return action.payload;
  }
  return state;
}

function actionBook(book: []) {
  return {
    type: 'BOOK_GET',
    payload: book
  }
}

function mapStateToProps(state: any) {
  return {
    book: state.book
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionBook: actionBook }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserBook)
