import * as React from 'react';
import './user-books.scss'
import { BooksService } from '../../../shared/services/books.service';
import { IBookModel } from '../../../../../../shared/models/book.model';
import { IAuthorModel } from '../../../../../../shared/models/author.model';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import Pagination from "react-js-pagination";
import { ReactNode } from 'react';
import { Link } from 'react-router-dom';

class UserBooks extends React.Component<any, any>  {
  public booksService = new BooksService();
  public currentPage: number = 1
  public counter: any
  public book: IBookModel = {
    _id: '',
    title: '',
    authors: [],
    img: '',
    magazine: '',
  }

  constructor(props: any) {
    super(props);
    this.state = {
      success: false,
      modalIsOpen: false,
      btnName: '',
      searchValue: '',
      itemsPerPage: 8,
    };
    // this.closeModal = this.closeModal.bind(this);
    this.editBook = this.editBook.bind(this);
    this.addBook = this.addBook.bind(this);
    this.deleteBook = this.deleteBook.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
  }

  componentWillMount() {
    this.loadBooks();
  }

  public loadBooks(): void {
    this.booksService.getBooks(this.state.itemsPerPage, this.currentPage).then((res: any) => {
      this.props.actionBooks(res.data.books, res.data.bookslength[0].length);
    })
  }

  public loadBooksBySearch(event: any): void {
    if (this.counter) {
      clearTimeout(this.counter);
    }
    this.setState({
      searchValue: event.target.value
    })
    this.counter = setTimeout(() => {
      this.booksService.getBooks(this.state.itemsPerPage, this.currentPage, event.target.value).then(
        res => {
          if (res.data.bookslength.length < 1) {
            this.props.actionBooks(res.data.books, 0)
            return;
          }
          this.props.actionBooks(res.data.books, res.data.bookslength[0].length)
        }
      );
    }, 500);
  }

  public addBook() {
    this.setState({
      modalIsOpen: true,
      btnName: 'Add'
    });
  }

  public editBook(book: IBookModel): void {
    this.book = book
    this.setState({
      modalIsOpen: true,
      btnName: 'Edit'
    });
  }

  public deleteBook(bookId: string): void {
    this.booksService.deleteBook(bookId).then(() => {
      this.loadBooks();
    })
  }

  public handlePageChange(pageNumber: number): void {
    this.currentPage = pageNumber
    this.loadBooks();
  }

  public showBooks(): ReactNode {
    return this.props.books.map((item: IBookModel, index: number) => {
      return (
        <div className="item" key={index}>
          <div className="item-img">
          <Link to={{ pathname: '/user/books/' + item._id}} ><img src={item.img} alt={item.img}/></Link> 
          </div>
          <div className="item-descr">
            <div>
              <Link to={{ pathname: '/user/books/' + item._id}} >
                <span className="item-title">{item.title.length > 15 ? (item.title.slice(0, 13)) + '...' : item.title}</span>
              </Link> 
            </div>
            <div className="item-name">
              {item.authors.map((author: IAuthorModel | any, index: number) => {
                return <p key={index}>
                  <span>{author.name.length > 20 ? (author.name.slice(0, 17)) + '...' : (author.name)}</span>
                </p>
              })}
            </div>
          </div>
          <div>
            <button className="btn-del" ><i onClick={() => this.deleteBook(item._id)} className="fas fa-trash-alt"></i></button>
          </div>
          <div>
            <button className="btn-edit"><i onClick={() => this.editBook(item)} className="fas fa-edit"></i></button>
          </div>
        </div>
      );
    })
  }

  public render() {
    let showSearchRes = { display: this.state.searchValue.length > 0 ? 'inline-block' : 'none' }
    return (
      <section className="container">
        <div className="books">
          <h2 className="title">Books</h2>
          <div className="col-10 item-list-box">
            <div className="search">
              <input
                type="text"
                placeholder="Find book"
                onChange={() => this.loadBooksBySearch(event)}
              />
              <i className="fas fa-search"></i>
              <p style={showSearchRes}>{this.props.bookslength} results for "{this.state.searchValue}"</p>
            </div>
            {this.showBooks()}
          </div>
          <div className="text-center">
            <Pagination
              activePage={this.currentPage}
              itemsCountPerPage={this.state.itemsPerPage}
              totalItemsCount={this.props.bookslength}
              pageRangeDisplayed={5}
              onChange={this.handlePageChange}
              activeClass='active'
            />
          </div>
        </div>
      </section>
    );
  }
}

const initialState = {
  books: [],
  bookslength: 0
}

export const booksReducer = (state = initialState, action: any) => {
  if (action.type === 'BOOKS_GET') {
    return action.payload;
  }
  return state;
}

function actionBooks(books: [], bookslength = 0) {
  return {
    type: 'BOOKS_GET',
    payload: { books: books, bookslength: bookslength }
  }
}

function mapStateToProps(state: any) {
  return {
    books: state.books.books,
    bookslength: state.books.bookslength
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionBooks: actionBooks }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserBooks)
  