import * as React from 'react';
import './user-author.scss'
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { AuthorService } from '../../../shared/services/author.service';
import { BooksService } from '../../../shared/services/books.service';
import { IBookModel } from '../../../../../../shared/models/book.model';

class UserAuthor extends React.Component<any, any>  {
  public authorService = new AuthorService()
  public booksService = new BooksService();
  constructor(props: any) {
    super(props)
  }
  componentWillMount() {
    this.getBooksByAuthor();
  }

  public getBooksByAuthor(): void {
    this.authorService.getAuthor(this.props.match.params.id).then((res: {data:[]}) => {
      this.props.actionAuthor(res.data)
      this.booksService.getBookByAuthor(this.props.match.params.id).then((resp: { data: [] }) => {
        this.props.actionBooksByAuthor(resp.data)
      });
    })
  }
  public render() {
    let showText = { display: this.props.booksByAuthor.length > 0 ? 'none' : 'block' };
    return (
      <section className="container">
        <div className="author">
          <h2 className="title">{this.props.author.name}</h2>
          <div className="item-list-box">
            <div style={showText}>{this.props.author.name} hasn't book</div>
            {this.props.booksByAuthor.map((book: IBookModel, index: number) => {
              return <div className="item"  key={index}>
              <div className="item-img" > <img src={book.img}/></div>
              <div className="item-descr">
                <div>
                  <Link to={{ pathname: '/user/books/' + book._id }}>
                  <span className="item-title">{book.title.length > 15 ? (book.title.slice(0, 13)) + '...' : book.title}</span>
                  </Link>
                </div>
              </div>
            </div >
            })}
          </div >
        </div>
      </section >
    );
  }
}

export const authorReducer = (state = [], action: any) => {
  if (action.type === 'AUTHOR_GET') {
    return action.payload;
  }
  return state;
}

function actionAuthor(author: []) {
  return {
    type: 'AUTHOR_GET',
    payload: author
  }
}

export const booksByAuthorReducer = (state = [], action: any) => {
  if (action.type === 'BOOKS_BY_AUTHOR_GET') {
    return action.payload;
  }
  return state;
}

function actionBooksByAuthor(booksByAuthor: []) {
  return {
    type: 'BOOKS_BY_AUTHOR_GET',
    payload: booksByAuthor
  }
}

function mapStateToProps(state: any) {
  return {
    author: state.author,
    booksByAuthor: state.booksByAuthor
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionAuthor: actionAuthor, actionBooksByAuthor: actionBooksByAuthor }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAuthor)
  