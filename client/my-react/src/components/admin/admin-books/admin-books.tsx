import * as React from 'react';
import './admin-books.scss'
import { BooksService } from '../../../shared/services/books.service';
import { AuthorService } from '../../../shared/services/author.service';
import { MagazineService } from '../../../shared/services/magazine.service';
import { IBookModel } from '../../../../../../shared/models/book.model';
import { IAuthorModel } from '../../../../../../shared/models/author.model';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import Pagination from "react-js-pagination";
import ReactModal from 'react-modal';
import { IMagazineModel } from '../../../../../../shared/models/magazine.model';
import { Link } from 'react-router-dom';
import { actionAuthors, actionMagazines } from '../../../actions/actions';
import { ReactNode } from 'react';

class AdminBooks extends React.Component<any, any>  {
  public booksService = new BooksService();
  public autorService = new AuthorService();
  public magazineService = new MagazineService();
  public currentPage: number = 1;
  public counter: any;
  public imgBase: string = '';
  public author: IAuthorModel = {
    _id: '',
    name: ''
  }
  public book: IBookModel = {
    _id: '',
    title: '',
    authors: [],
    img: '',
    magazine: '',
  }

  constructor(props: any) {
    super(props);
    this.state = {
      success: false,
      modalIsOpen: false,
      btnName: '',
      searchValue: '',
      itemsPerPage: 8,
      err: '',
      chooseAuthors: []
    };
    this.closeModal = this.closeModal.bind(this);
    this.editBook = this.editBook.bind(this);
    this.addBook = this.addBook.bind(this);
    this.deleteBook = this.deleteBook.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleFile = this.handleFile.bind(this);
  }

  componentWillMount() {
    this.loadBooks();
    this.loadAuthors();
  }

  public loadBooks(): void {
    this.booksService.getBooks(this.state.itemsPerPage, this.currentPage).then(res => {
      this.props.actionBooks(res.data.books, res.data.bookslength[0].length);
    })
  }

  public loadAuthors(): void {
    this.autorService.getAuthors().then((res: { data: [] }) => {
      this.props.actionAuthors(res.data);
    })
  }

  public loadMagazines(): void {
    this.magazineService.getMagazines().then((res: { data: [] }) => {
      this.props.actionMagazines(res.data);
    })
  }

  public loadBooksBySearch(event: any): void {
    if (this.counter) {
      clearTimeout(this.counter);
    }
    this.setState({
      searchValue: event.target.value
    })
    this.counter = setTimeout(() => {
      this.booksService.getBooks(this.state.itemsPerPage, this.currentPage, event.target.value).then(
        res => {
          console.log(res)
          if (res.data.bookslength.length < 1) {
            this.props.actionBooks(res.data.books, 0)
            return;
          }
          this.props.actionBooks(res.data.books, res.data.bookslength[0].length)
        }
      );
    }, 500);
  }

  public addBook(): void {
    this.setState({
      modalIsOpen: true,
      btnName: 'Add'
    });
    this.loadMagazines();
  }

  public editBook(book: IBookModel): void {
    this.book = book
    this.setState({
      modalIsOpen: true,
      btnName: 'Edit'
    });
    this.book.authors.map((author: IAuthorModel | any) => {
      this.state.chooseAuthors.push(author);
    });
    this.loadMagazines();
  }

  public deleteBook(bookId: string): void {
    this.booksService.deleteBook(bookId).then(() => {
      this.loadBooks();
    })
  }

  public handlePageChange(pageNumber: number): void {
    this.currentPage = pageNumber
    this.loadBooks();
  }

  public handleFile(event: any): void {
    let file = event.target.files[0];
    let uploadImg = (image: string) => {
      this.book.img = image;
    }
    let myReader = new FileReader();
    myReader.onloadend = (e) => {
      let img = e.target as any;
      uploadImg(img.result);
    };
    myReader.readAsDataURL(file);
  }

  public chooseAuthor(event: any): void {
    this.setState({
      err: ''
    })
    const bool = this.state.chooseAuthors.find((item: IAuthorModel) => {
      return item._id === event.target.value;
    });
    if (bool === undefined) {
      this.props.authors.map((author: IAuthorModel) => {
        if (author._id === event.target.value) {
          this.setState({
            chooseAuthors: [...this.state.chooseAuthors, author]
          })
        }
      });
      return;
    }
    this.setState({
      err: 'Author has been already add'
    })
  }

  public deleteAuthor(event: any, ind: number): void {
    event.preventDefault();
    const chooseAuthors = [...this.state.chooseAuthors];
    if (ind !== -1) {
      chooseAuthors.splice(ind, 1);
      this.setState({
        err: '',
        chooseAuthors: chooseAuthors
      });
    }
  }

  public submit(event: any, id: string): void {
    event.preventDefault();
    setTimeout(() => { this.closeModal(); }, 3000);
    const body: IBookModel = {
      _id: id || null,
      title: this.book.title,
      authors: this.state.chooseAuthors,
      img: this.book.img,
      magazine: (() => {
        if (this.book.magazine === '') {
          return null;
        }
        if (typeof (this.book.magazine) === "object") {
          this.book.magazine.map((magazine: IMagazineModel) => {
            return magazine._id
          })
          return
        }
        return this.book.magazine
      })()
    };
    if (this.state.btnName === 'Edit') {
      this.booksService.updateBook(body, id).then(() => { });
      this.setState({
        success: true
      });
      return;
    }
    this.booksService.addBook(body).then(() => {
      this.setState({
        success: true
      });
    });
  }

  public closeModal(): void {
    this.setState({
      modalIsOpen: false,
      success: false,
      chooseAuthors: []
    });
    this.book = {
      _id: '',
      title: '',
      authors: [],
      img: '',
      magazine: '',
    }
    this.loadBooks();
  }

  public showBooks(): ReactNode {
    return this.props.books.map((item: IBookModel, index: number) => {
      return (
        <div className="item" key={index}>
          <div className="item-img">
            <Link to={{ pathname: '/admin/books/' + item._id }} ><img src={item.img} alt={item.img} /></Link>
          </div>
          <div className="item-descr">
            <div>
              <Link to={{ pathname: '/admin/books/' + item._id }} >
                <span className="item-title">{item.title.length > 15 ? (item.title.slice(0, 13)) + '...' : item.title}</span>
              </Link>
            </div>
            <div className="item-name">
              {item.authors.map((author: IAuthorModel | any, index: number) => {
                return <p key={index}>
                  <span>{author.name.length > 20 ? (author.name.slice(0, 17)) + '...' : (author.name)}</span>
                </p>
              })}
            </div>
          </div>
          <div>
            <button className="btn-del" ><i onClick={() => this.deleteBook(item._id)} className="fas fa-trash-alt"></i></button>
          </div>
          <div>
            <button className="btn-edit"><i onClick={() => this.editBook(item)} className="fas fa-edit"></i></button>
          </div>
        </div>
      );
    })
  }

  public render() {
    let showForm = { display: this.state.success ? 'none' : 'inline-block' };
    let showSuccess = { display: this.state.success ? 'inline-block' : 'none' };
    let showSearchRes = { display: this.state.searchValue.length > 0 ? 'inline-block' : 'none' }
    return (
      <section className="container">
        <div className="books">
          <h2 className="title">Books</h2>
          <div className="add-box-btn">
            <button onClick={() => this.addBook()}><i className="fas fa-plus"></i></button>
          </div>
          <div className="col-10 item-list-box">
            <div className="search">
              <input
                type="text"
                placeholder="Find book"
                onChange={() => this.loadBooksBySearch(event)}
              />
              <i className="fas fa-search"></i>
              <p style={showSearchRes}>{this.props.bookslength} results for "{this.state.searchValue}"</p>
            </div>
            {this.showBooks()}
          </div>
          <div className="text-center">
            <Pagination
              activePage={this.currentPage}
              itemsCountPerPage={this.state.itemsPerPage}
              totalItemsCount={this.props.bookslength}
              pageRangeDisplayed={5}
              onChange={this.handlePageChange}
              activeClass='active'
            />
          </div>
          <ReactModal
            className="popup"
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            contentLabel="BookModal"
          >
            <div className="popup-box">
              <form className="form-book" onSubmit={() => this.submit(event, this.book._id)} style={showForm}>
                <div className="form-box">
                  <div className="form-group">
                    <label htmlFor="file-upload" className="custom-file-upload">
                      <i className="fas fa-image"></i>
                    </label>
                    <input
                      id="file-upload"
                      type="file"
                      onChange={() => this.handleFile(event)}
                    />
                  </div>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Title"
                      defaultValue={this.book.title}
                      onChange={(event) => this.book.title = event.target.value}
                    />
                    <span><i className="fas fa-book"></i></span>
                  </div>
                  <div className="form-group">
                    <select
                      onChange={() => this.chooseAuthor(event)}
                    >
                      <option value=""></option>
                      {this.props.authors.map((author: IAuthorModel, index: number) => {
                        return <option value={author._id} key={index}>
                          {author.name}
                        </option>
                      })}
                    </select>
                    <span><i className="far fa-user"></i></span>
                    <p className="err">{this.state.err}</p>
                    {this.state.chooseAuthors.map((author: IAuthorModel, index: number) => {
                      return <p className="author-name" key={index}>
                        {author.name}
                        <button onClick={() => this.deleteAuthor(event, index)} className="del-author">
                          <i className="far fa-times-circle"></i>
                        </button>
                      </p>
                    })}
                  </div>
                  <div className="form-group">
                    <select
                      onChange={(event) => this.book.magazine = event.target.value}

                    >
                      <option value=""></option>
                      {this.props.magazines.map((magazine: IMagazineModel, index: number) => {
                        return <option value={magazine._id} key={index}>
                          {magazine.title}
                        </option>
                      })}
                    </select>
                    <span><i className="fas fa-book-open"></i></span>
                  </div>
                  <div className="form-group">
                    <button
                      type="submit"
                      className="btn">
                      {this.state.btnName}
                    </button>
                  </div>
                </div>
              </form>
              <div className="my-form" style={showSuccess}>
                <div className="form-box">
                  <div className="success">User has been {this.state.btnName.toLowerCase()}<i className="fas fa-check-circle"></i></div>
                </div>
              </div>
              <button className="btn-close" onClick={this.closeModal}><i className="fas fa-times"></i></button>
            </div>
          </ReactModal>
        </div>
      </section>
    );
  }
}

const initialState = {
  books: [],
  bookslength: 0
}

export const booksReducer = (state = initialState, action: any) => {
  if (action.type === 'BOOKS_GET') {
    return action.payload;
  }
  return state;
}

function actionBooks(books: [], bookslength = 0) {
  return {
    type: 'BOOKS_GET',
    payload: { books: books, bookslength: bookslength }
  }
}

function mapStateToProps(state: any) {
  return {
    books: state.books.books,
    bookslength: state.books.bookslength,
    authors: state.authors,
    magazines: state.magazines
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionBooks: actionBooks, actionAuthors: actionAuthors, actionMagazines: actionMagazines }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminBooks)
