import * as React from 'react';
import './confirm-users.scss';
import { AdminService } from '../../../shared/services/admin.service';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { ReactNode } from 'react';
import { IUserModel } from '../../../../../../shared/models/user.model';

class ConfirmUsers extends React.Component<any, any>  {
  public adminService = new AdminService;
  constructor(props: any) {
    super(props);
    this.state = {
      isNoUser: true
    };
    this.getNoConfirmUsers = this.getNoConfirmUsers.bind(this);
    this.confirmUser = this.confirmUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
    this.showUsers = this.showUsers.bind(this);
  }

  componentWillMount() {
    this.getNoConfirmUsers();
  }

  public getNoConfirmUsers(): void {
    this.adminService.getNoConfirmUsers().then((res: { data: [] }) => {
      this.props.actionUser(res.data)
      if (res.data.length > 0) {
        this.setState({
          isNoUser: false
        });
        return
      }
      this.setState({
        isNoUser: true
      });
    })
  }

  public confirmUser(id: string): void {
    this.adminService.updateConfirmUser(id).then(() => {
      this.getNoConfirmUsers();
    });
  }

  public deleteUser(id: string): void {
    this.adminService.deleteUser(id).then(() => {
      this.getNoConfirmUsers();
    });
  }

  public showUsers(): ReactNode {
    return this.props.users.map((user: IUserModel, index: any) => {
      return (
        <div className="item" key={index}>
          <span>{index + 1}. {user.fullName}</span>
          <span>{user.email}</span>
          <div className="box-btn">
            <span>Confirm: </span>
            <button className="btn-add"><i onClick={() => this.confirmUser(user._id)} className="fas fa-clipboard-check"></i></button>
            <button className="btn-del" ><i onClick={() => this.deleteUser(user._id)} className="fas fa-user-minus"></i></button>
          </div >
        </div>
      );
    })
  }

  public render() {
    let showUser = { display: this.state.isNoUser ? 'none' : 'block' };
    let showNoUser = { display: this.state.isNoUser ? 'block' : 'none' };
    return (
      <section className="container">
        <div className="confirm-user">
          <h2 style={showNoUser}>No new users</h2>
          <h2 style={showUser}> Add new users: {this.props.users.length} </h2 >
          <div className="col-10 item-list-box">
            {this.showUsers()}
          </div >
        </div>
      </section >
    );
  }
}

export const usersReducer = (state = [], action: any) => {
  if (action.type === 'USERS_GET') {
    return action.payload
  }
  return state;
}

function actionUser(users: []) {
  return {
    type: 'USERS_GET',
    payload: users
  }
}

function mapStateToProps(state: any) {
  return {
    users: state.users
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionUser: actionUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmUsers)
