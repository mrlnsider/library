import * as React from 'react';
import { MagazineService } from '../../../shared/services/magazine.service';
import { bindActionCreators, Dispatch, Action } from 'redux';
import { connect } from 'react-redux';
import { IMagazineModel } from '../../../../../../shared/models/magazine.model';
import { actionMagazines } from '../../../actions/actions';
import ReactModal from 'react-modal';
import { ReactNode } from 'react';

class AdminMagazines extends React.Component<any, any>  {
  public magazineService = new MagazineService()
  public magazine: IMagazineModel = {
    _id: '',
    title: '',
    publisher: '',
    img: ''
  }
  constructor(props: any) {
    super(props);
    this.state = {
      success: false,
      modalIsOpen: false,
      btnName: '',
      err: '',
    };
    this.closeModal = this.closeModal.bind(this);
    this.editMagazine = this.editMagazine.bind(this);
    this.addMagazine = this.addMagazine.bind(this);
    this.deleteMagazine = this.deleteMagazine.bind(this);
  }
  componentWillMount() {
    this.loadMagazines();
  }
  public loadMagazines(): void {
    this.magazineService.getMagazines().then(
      (res: { data: [] }) => {
        this.props.actionMagazines(res.data)
      }
    );
  }

  public addMagazine(): void {
    this.setState({
      modalIsOpen: true,
      btnName: 'Add'
    });
  }

  public editMagazine(magazine: IMagazineModel): void {
    this.magazine = magazine;
    this.setState({
      btnName: 'Edit',
      modalIsOpen: true,
    })
  }

  public deleteMagazine(id: string): void {
    this.magazineService.deleteMagazine(id).then(() => {
      this.loadMagazines();
    });
  }

  public handleFile(event: any): void {
    let file = event.target.files[0];
    let uploadImg = (image: string) => {
      this.magazine.img = image;
    }
    let myReader = new FileReader();
    myReader.onloadend = (e) => {
      let img = e.target as any;
      uploadImg(img.result);
    };
    myReader.readAsDataURL(file);
  }

  public submit(event: any, id: string): void {
    event.preventDefault();
    setTimeout(() => { this.closeModal(); }, 3000);
    const body: IMagazineModel = {
      _id: id || null,
      title: this.magazine.title,
      img: this.magazine.img,
      publisher: this.magazine.publisher
    };
    if (this.state.btnName === 'Edit') {
      this.magazineService.updateMagazine(body, id).then(() => { });
      this.setState({
        success: true
      });
      return;
    }
    this.magazineService.addMagazine(body).then(() => {
      this.setState({
        success: true
      });
    });
  }

  public closeModal(): void {
    this.setState({
      modalIsOpen: false,
      success: false,
    });
    this.magazine = {
      _id: '',
      title: '',
      publisher: '',
      img: ''
    }
    this.loadMagazines();
  }

  public showMagazines(): ReactNode {
    return this.props.magazines.map((magazine: IMagazineModel, index: number) => {
      return (
        <div className="item" key={index}>
          <div className="item-img"> <img src={magazine.img} /></div>
          <div className="item-descr">
            <div>
              <span className="item-title">{magazine.title.length > 15 ? (magazine.title.slice(0, 13)) + '...' : magazine.title}</span>
            </div>
            <div className="item-publisher">
              <span>{magazine.publisher}</span>
            </div>
          </div>
          <div>
            <button className="btn-del" onClick={() => this.deleteMagazine(magazine._id)}><i className="fas fa-trash-alt"></i></button>
          </div>
          <div>
            <button className="btn-edit" onClick={() => this.editMagazine(magazine)}><i className="fas fa-edit"></i></button>
          </div>
        </div>
      );
    })
  }

  public render() {
    let showForm = { display: this.state.success ? 'none' : 'inline-block' };
    let showSuccess = { display: this.state.success ? 'inline-block' : 'none' };
    return (
      <section className="container">
        <div className="books">
          <h2 className="title">Magazines</h2>
          <div className="add-box-btn">
            <button onClick={() => this.addMagazine()}><i className="fas fa-plus"></i></button>
          </div>
          <div className="col-10 item-list-box">
            {this.showMagazines()}
          </div>
          <ReactModal
            className="popup"
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            contentLabel="BookModal"
          >
            <div className="popup-box">
              <form className="form-book" onSubmit={() => this.submit(event, this.magazine._id)} style={showForm}>
                <div className="form-box">
                  <div className="form-group">
                    <label htmlFor="file-upload" className="custom-file-upload">
                      <i className="fas fa-image"></i>
                    </label>
                    <input
                      id="file-upload"
                      type="file"
                      onChange={() => this.handleFile(event)}
                    />
                  </div>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Title"
                      defaultValue={this.magazine.title}
                      onChange={(event) => this.magazine.title = event.target.value}
                    />
                    <span><i className="fas fa-book"></i></span>
                  </div>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Editor-in-chief"
                      defaultValue={this.magazine.publisher}
                      onChange={(event) => this.magazine.publisher = event.target.value}
                    />
                    <span><i className="far fa-user"></i></span>
                  </div>
                  <div className="form-group">
                    <button
                      type="submit"
                      className="btn">
                      {this.state.btnName}
                    </button>
                  </div>
                </div>
              </form>
              <div className="my-form" style={showSuccess}>
                <div className="form-box">
                  <div className="success">User has been {this.state.btnName.toLowerCase()}<i className="fas fa-check-circle"></i></div>
                </div>
              </div>
              <button className="btn-close" onClick={this.closeModal}><i className="fas fa-times"></i></button>
            </div>
          </ReactModal>
        </div>
      </section>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    magazines: state.magazines
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionMagazines: actionMagazines }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminMagazines)
