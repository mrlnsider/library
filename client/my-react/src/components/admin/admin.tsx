import * as React from 'react';
import AdminHeader from '../../containers/admin-header';
import { Switch, Route } from 'react-router';
import ConfirmUsers from './confirm-users';
import AdminBooks from './admin-books';
import UsersList from './users-list';
import { NotFound } from '../../App';
import AdminBook from './admin-books/admin-book';
import AuthorList from './author-list';
import Author from './author-list/author';
import AdminMagazines from './admin-magazines';


class Admin extends React.Component<any, any>  {
  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <React.Fragment>
        <AdminHeader item={this.props} />
        <main>
          <Switch>
            <Route path='/admin' exact component={ConfirmUsers} />
            <Route path='/admin/users' component={UsersList} />
            <Route path='/admin/books' component={AdminBooks} exact />
            <Route path="/admin/books/:id" component={AdminBook} exact />
            <Route path='/admin/authors' component={AuthorList} />
            <Route path='/admin/author/:id' component={Author} />
            <Route path='/admin/magazines' component={AdminMagazines} />
            <Route to="*" component={NotFound} exact />
          </Switch>
        </main>
      </React.Fragment>
    );
  }
}

export default Admin;
