import * as React from 'react';
import './author-list.scss'
import { AuthorService } from '../../../shared/services/author.service';
import { IAuthorModel } from '../../../../../../shared/models/author.model';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { actionAuthors } from '../../../actions/actions';
import ReactModal from 'react-modal';
import { Link } from 'react-router-dom';

class AuthorList extends React.Component<any, any>  {
  public authorService: AuthorService = new AuthorService();
  public author: IAuthorModel = {
    _id: '',
    name: ''
  }
  constructor(props: any) {
    super(props)
    this.state = {
      toggleAdd: false,
      inputValue: '',
      modalIsOpen: false,
    }
    this.toggleShowAdd = this.toggleShowAdd.bind(this);
    this.deleteAuthor = this.deleteAuthor.bind(this);
    this.addAuthor = this.addAuthor.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.editAuthor = this.editAuthor.bind(this);
  }

  public updateInputValue(evt: any) {
    this.setState({
      inputValue: evt.target.value
    });
  }

  componentWillMount() {
    this.loadAuthors();
  }

  public loadAuthors(): void {
    this.authorService.getAuthors().then(
      (res: { data: [] }) => {
        this.props.actionAuthors(res.data) as IAuthorModel[];
      }
    );
  }

  public toggleShowAdd(): void {
    this.setState({
      toggleAdd: !this.state.toggleAdd
    })

  }

  public editAuthor(author: IAuthorModel): void {
    this.author = author
    this.setState({
      modalIsOpen: true,
    });
  }

  public addAuthor(): void {
    this.author.name = this.state.inputValue
    this.authorService.addAuthor(this.author).then(() => {
      this.setState({
        inputValue: ''
      })
      this.loadAuthors();
    });
  }

  public deleteAuthor(id: string): void {
    this.authorService.deleteAuthor(id).then(() => {
      this.loadAuthors();
    });
  }

  public closeModal(): void {
    this.setState({
      modalIsOpen: false,
    });
    this.author = {
      _id: '',
      name: ''
    }
    this.loadAuthors();
  }

  public submit(event: any): void {
    event.preventDefault();
    this.authorService.updateAuthor(this.author).then(() => {
      this.closeModal()
    })
    this.loadAuthors();
  }



  public render() {
    let showAddButton = { display: this.state.toggleAdd ? 'block' : 'none' };
    return (
      <section className="container">
        <div className="author-list">
          <h2 className="title">Authors</h2>
          <div className="add-box-btn">
            <button onClick={() => this.toggleShowAdd()} ><i className="fas fa-plus"></i></button>
          </div>
          <div className="add-item" style={showAddButton} >
            <input
              type="text"
              className="inp"
              placeholder="Author"
              value={this.state.inputValue}
              onChange={() => this.updateInputValue(event)}
            />
            <span><i className="fas fa-user"></i></span>
            <button className="add-btn" onClick={() => this.addAuthor()}>Add</button>
          </div>
          <div className="item-list-box">
            {this.props.authors.map((author: IAuthorModel, index: number) => {
              return <div className="item" key={index} >
                <div className="item-descr">Author:&nbsp;
                  <Link to={{ pathname: '/admin/author/' + author._id }}>
                    <span className="route">{author.name}</span>
                  </Link>
                </div>
                <div>
                  <button className="show-edit" onClick={() => this.editAuthor(author)}><i className="fas fa-edit"></i></button>
                  <button className="btn-del" onClick={() => this.deleteAuthor(author._id)} ><i className="fas fa-trash-alt"></i></button>
                </div>
              </div>
            })}
          </div>
        </div>
        <ReactModal
          className="popup"
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
          contentLabel="BookModal"
        >
          <div className="popup-box">
            <form className="form-book" onSubmit={() => this.submit(event)}>
              <div className="form-box">
                <div className="form-group">
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Title"
                    onChange={(event) => this.author.name = event.target.value}
                    defaultValue={this.author.name}
                  />
                  <span><i className="far fa-user"></i></span>
                </div>
                <div className="form-group">
                  <button
                    type="submit"
                    className="btn">
                    Edit
                  </button>
                </div>
              </div>
            </form>
            <button className="btn-close" onClick={this.closeModal}><i className="fas fa-times"></i></button>
          </div>
        </ReactModal>
      </section>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    authors: state.authors
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionAuthors: actionAuthors }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorList)
