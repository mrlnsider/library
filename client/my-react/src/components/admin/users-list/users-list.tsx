import * as React from 'react';
import './users-list.scss'
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { UserRole } from '../../../shared/enam/userRole.enam';
import ReactModal from 'react-modal';
import { AdminService } from '../../../shared/services/admin.service';
import { IUserModel } from '../../../../../../shared/models/user.model';
import { ReactNode } from 'react';

ReactModal.setAppElement('body')

class UsersList extends React.Component<any, any>  {
  public adminService = new AdminService();
  public userRole = UserRole
  public user: IUserModel = {
    _id: '',
    email: '',
    fullName: '',
    password: '',
    confirm: true,
    role: 1
  }
  constructor(props: any) {
    super(props);
    this.state = {
      success: false,
      modalIsOpen: false,
      btnName: ''
    };
    this.closeModal = this.closeModal.bind(this);
    this.editUser = this.editUser.bind(this);
    this.addUser = this.addUser.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
  }

  componentWillMount() {
    this.getConfirmUsers();
  }

  public getConfirmUsers(): void {
    this.adminService.getConfirmUsers().then((res: { data: [] }) => {
      this.props.actionConfirmUsers(res.data);
    })
  }

  public addUser() {
    this.setState({
      modalIsOpen: true,
      btnName: 'Add'
    });
  }

  public editUser(user: any): any {
    this.user = user
    this.setState({
      modalIsOpen: true,
      btnName: 'Edit'
    });
  }

  public deleteUser(userId: any) {
    this.adminService.deleteUser(userId).then(() => {
      this.getConfirmUsers();
    })
  }

  closeModal() {
    this.setState({
      modalIsOpen: false,
      success: false
    });
    this.user = {
      _id: '',
      email: '',
      fullName: '',
      password: '',
      confirm: true,
      role: 1
    }
    this.getConfirmUsers();
  }

  public submit(event: any, id: string): void {
    event.preventDefault();
    setTimeout(() => { this.closeModal(); }, 3000);
    if (this.state.btnName === 'Edit') {
      this.adminService.updateUser(this.user, id).then(() => { });
      this.setState({
        success: true
      });
      return;
    }
    this.adminService.addUser(this.user).then(() => {
      this.setState({
        success: true
      });
    });
  }


  public showUsers(): ReactNode {
    return this.props.confirmUsers.map((user: IUserModel | any, index: any) => {
      return (
        <div className="item" key={index}>
          <div className="item-box">
            <div className="icon"><i className="fas fa-user"></i></div>
            <div>{user.email}</div>
            <div>Name: {user.fullName}</div>
            <div>Confirm: {String(user.confirm)}</div>
            <div>Status: {this.userRole[user.role]} </div>
          </div>
          <div>
            <button className="btn-del" ><i onClick={() => this.deleteUser(user._id)} className="fas fa-trash-alt"></i></button>
          </div>
          <div>
            <button className="btn-edit"><i onClick={() => this.editUser(user)} className="fas fa-user-edit"></i></button>
          </div>
        </div>
      );
    })
  }

  public render() {
    let showForm = { display: this.state.success ? 'none' : 'inline-block' };
    let showSuccess = { display: this.state.success ? 'inline-block' : 'none' };
    return (
      <section className="container">
        <div className="users">
          <h2 className="title">All Users</h2>
          <div className="add-box-btn">
            <button onClick={() => this.addUser()} ><i className="fas fa-plus"></i></button>
          </div>
          <div className="col-10 item-list-box">
            {this.showUsers()}
          </div>
          <ReactModal
            className="popup"
            isOpen={this.state.modalIsOpen}
            onRequestClose={this.closeModal}
            contentLabel="UserModal"
          >
            <div className="popup-box">
              <form className="my-form" onSubmit={() => this.submit(event, this.user._id)} style={showForm}>
                <div className="form-box">
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Full Name"
                      onChange={(event) => this.user.fullName = event.target.value}
                      defaultValue={this.user.fullName}
                    />
                    <span><i className="fas fa-user"></i></span>
                  </div>
                  <div className="form-group">
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Email"
                      defaultValue={this.user.email}
                      onChange={(event) => this.user.email = event.target.value}
                    />
                    <span><i className="fas fa-envelope"></i></span>
                  </div>
                  <div className="form-group">
                    <input
                      type="password"
                      className="form-control"
                      placeholder="Password"
                      onChange={(event) => this.user.password = event.target.value}
                    />
                    <span><i className="fas fa-unlock-alt"></i></span>
                  </div>
                  <div className="form-group">
                    <select
                      defaultValue={String(this.user.role)}
                      onChange={(event) => this.user.role = +event.target.value}
                    >
                      <option value='1'>User</option>
                      <option value='2'>Admin</option>
                    </select>
                    <span><i className="fas fa-crown"></i></span>
                  </div>
                  <div className="form-group">
                    <select
                      defaultValue={String(this.user.confirm)}
                      onChange={(event) => { let confirm; event.target.value === 'true' ? confirm = true : confirm = false; this.user.confirm = confirm }}
                    >
                      <option value='true'>true</option>
                      <option value='false'>false</option>
                    </select>
                    <span><i className="fas fa-check-square"></i></span>
                  </div>
                  <div className="form-group">
                    <button
                      type="submit"
                      className="btn">
                      {this.state.btnName}
                    </button>
                  </div>
                </div>
              </form>
              <div className="my-form" style={showSuccess}>
                <div className="form-box">
                  <div className="success">User has been {this.state.btnName.toLowerCase()}<i className="fas fa-check-circle"></i></div>
                </div>
              </div>
              <button className="btn-close" onClick={this.closeModal}><i className="fas fa-times"></i></button>
            </div>
          </ReactModal>
        </div>
      </section>
    );
  }
}

export const confirmUsersReducer = (state = [], action: any) => {
  if (action.type === 'CONFIRM_USERS_GET') {
    return action.payload
  }
  return state;
}

function actionConfirmUsers(confirmUsers: []) {
  return {
    type: 'CONFIRM_USERS_GET',
    payload: confirmUsers
  }
}

function mapStateToProps(state: any) {
  return {
    confirmUsers: state.confirmUsers
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators({ actionConfirmUsers: actionConfirmUsers }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersList)
