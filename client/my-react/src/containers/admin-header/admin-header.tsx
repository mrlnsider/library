import * as React from 'react';
import './admin-header.scss';
import { NavLink, Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { actionRole, actionAuth } from '../../actions/actions';
import { connect } from 'react-redux';

class AdminHeader extends React.Component<any, any>  {
  public logoUrl = 'https://upload.wikimedia.org/wikipedia/ru/thumb/5/50/Queens_Library_logo.svg/1280px-Queens_Library_logo.svg.png';

  public logOut():void {
    localStorage.clear();
    this.props.actionAuth(false);
    this.props.actionRole(0)
    this.props.item.history.push('/auth');
  }
  
  public render() {
    return (
      <header>
        <div className="container">
          <img src={this.logoUrl}/>
          <nav>
            <ul>
              <li><NavLink activeClassName="active" to='/admin' exact>Confirm</NavLink></li>
              <li><NavLink activeClassName="active" to='/admin/users'>Users</NavLink></li>
              <li><NavLink activeClassName="active" to='/admin/books'>Books</NavLink></li>
              <li><NavLink activeClassName="active" to='/admin/authors'>Authors</NavLink></li>
              <li><NavLink activeClassName="active" to='/admin/magazines'>Magazines</NavLink></li>
            </ul>
          </nav>
          <ul className="logout">
            <button onClick={this.logOut.bind(this)}><i className="fas fa-sign-out-alt"></i></button>
          </ul>
        </div>
      </header>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    role: state.role
  }
}
function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({actionAuth: actionAuth, actionRole: actionRole}, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(AdminHeader)