import * as React from 'react';
import './user-header.scss'
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionAuth, actionRole } from '../../actions/actions';

class UserHeader extends React.Component<any, any>  {
  public logoUrl = 'https://upload.wikimedia.org/wikipedia/ru/thumb/5/50/Queens_Library_logo.svg/1280px-Queens_Library_logo.svg.png';

  public logOut():void {
    localStorage.clear();
    this.props.actionAuth(false);
    this.props.actionRole(0)
    this.props.item.history.push('/auth');
  }

  public render() {
    return (
      <header>
        <div className="container">
          <img src={this.logoUrl}/>
          <nav>
            <ul>
              <li><NavLink activeClassName="active" to='/user/books'>Books</NavLink></li>
            </ul>
          </nav>
          <ul className="profile">
            <li><NavLink activeClassName="active" to='/user'><i className="fas fa-user"></i></NavLink></li>
            <span>or</span>
            <li><button onClick={this.logOut.bind(this)}><i className="fas fa-sign-out-alt"></i></button></li>
          </ul>
        </div>
      </header>
    );
  }
}

function mapStateToProps(state: any) {
  return {
    auth: state.auth,
    role: state.role
  }
}

function mapDispatchToProps(dispatch: any) {
  return bindActionCreators({actionAuth: actionAuth, actionRole: actionRole}, dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(UserHeader)
  