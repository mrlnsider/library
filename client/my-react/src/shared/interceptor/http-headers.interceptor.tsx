import axios from 'axios';

export class BaseService {
    constructor() {
        const token = localStorage.getItem('token')
        if(token) {
            axios.defaults.headers.common = {'x-access-token' : token}
        }
      
    }
}
