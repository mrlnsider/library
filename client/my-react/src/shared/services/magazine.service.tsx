import axios, { AxiosPromise } from 'axios';
import { environment } from '../../environments/environment';
import { BaseService } from '../interceptor/http-headers.interceptor';
import { IMagazineModel } from '../../../../../shared/models/magazine.model';

export class MagazineService extends BaseService {
    constructor() {
        super()
    }

    public getMagazines(): AxiosPromise {
        return axios.get(environment.url + '/magazine');
    }

    public getMagazine(id: string): AxiosPromise {
        return axios.get(environment.url + '/magazine/' + id);
    }

    public addMagazine(magazine: IMagazineModel): AxiosPromise {
        return axios.post(environment.url + '/magazine', magazine);
    }

    public updateMagazine(magazine: IMagazineModel, id: string): AxiosPromise {
        return axios.put(environment.url + '/magazine/' + id, magazine);
    }

    public deleteMagazine(id: string): AxiosPromise {
        return axios.delete(environment.url + '/magazine/' + id);
    }
}