import axios, { AxiosPromise } from 'axios';
import { environment } from '../../environments/environment';
import { BaseService } from '../interceptor/http-headers.interceptor';
import { IAuthorModel } from '../../../../../shared/models/author.model';

export class AuthorService extends BaseService {
    constructor() {
        super()
    }

public getAuthors(): AxiosPromise {
    return axios.get(environment.url + '/author');
  }

  public getAuthor(id: string): AxiosPromise {
    return axios.get(environment.url + '/author/' + id);
  }

  public addAuthor(author: IAuthorModel): AxiosPromise {
    const body = {
      name: author.name
    };
    return axios.post(environment.url + '/author', body);
  }

  public updateAuthor(author: IAuthorModel): AxiosPromise {
    const body = {
      _id: author._id,
      name: author.name
    };
    return axios.put(environment.url + '/author/' + author._id, body);
  }

  public deleteAuthor(id: string): AxiosPromise {
    return axios.delete(environment.url + '/author/' + id);
  }
}