import axios, { AxiosPromise } from 'axios';
import { environment } from '../../environments/environment';
import { BaseService } from '../interceptor/http-headers.interceptor';
import { IUserModel } from '../../../../../shared/models/user.model';

export class AdminService extends BaseService {
    constructor() {
      super()
    }
  
    public getNoConfirmUsers(): AxiosPromise {
        return axios.get(environment.url + '/admin/confirm');
    }

    public getConfirmUsers(): AxiosPromise {
      return axios.get(environment.url + '/admin/users');
    }

    public addUser(user: IUserModel): AxiosPromise {
      const body = {
        confirm: user.confirm,
        email: user.email,
        fullName: user.fullName,
        password: user.password,
        role: user.role
      };
      return axios.post(environment.url + '/admin/users', body);
    }
  
    public updateUser(user: IUserModel, id: string): AxiosPromise {
      const body = {
        confirm: user.confirm,
        email: user.email,
        fullName: user.fullName,
        password: user.password,
        role: user.role
      };
      return axios.put(environment.url + '/admin/users/' + id, body);
    }
  
    public updateConfirmUser(id: string): AxiosPromise {
      const body = { confirm: true };
      return axios.put(environment.url + '/admin/users/' + id, body);
    }
  
    public deleteUser(id: string): AxiosPromise {
      return axios.delete(environment.url + '/admin/users/' + id);
    }

    
}