import axios, { AxiosPromise } from 'axios';
import { environment } from '../../environments/environment';
import { BaseService } from '../interceptor/http-headers.interceptor';

export class UserService extends BaseService {
  constructor() {
    super()
  }

  public getUserInfo(): AxiosPromise {
    return axios.get(environment.url + '/auth/profile');
  }

  public getUserById(id: string): AxiosPromise {
    return axios.get(environment.url + '/user/' + id);
  }

  public updateUser(user: object, id: string): AxiosPromise {
    return axios.put(environment.url + '/user/' + id, user);
  }
  public updatePass(oldPass: string, newPass: string, id: string): AxiosPromise {
    const body = {
      oldPass: oldPass,
      newPass: newPass
    };
    return axios.put(environment.url + '/user/pass/' + id, body);
  }
}

