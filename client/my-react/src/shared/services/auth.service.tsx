import axios, { AxiosPromise } from 'axios';
import { environment } from '../../environments/environment';
import { IUserModel } from '../../../../../shared/models/user.model';

export class AuthService {
    constructor() {
      
    }
  
    public login(user: object): AxiosPromise {
        return axios.post(environment.url + '/auth/login', user);
    }
    
    public register(user: object): AxiosPromise {
        return axios.post(environment.url + '/auth/register', user);
    }
}