import jwt_decode from 'jwt-decode';

export class LocalStorageService {
    constructor() {
      
    }
  
    public getToken(): string {
        const token = localStorage.getItem('token');
        return token ? token : ''
    }
    
    public decodeToken(): object | null {
        return this.getToken() ? jwt_decode(this.getToken()) : null
    }
}