import axios, { AxiosPromise } from 'axios';
import { environment } from '../../environments/environment';
import { BaseService } from '../interceptor/http-headers.interceptor';
import { IBookModel } from '../../../../../shared/models/book.model';

export class BooksService extends BaseService {
    constructor() {
        super()
    }

    public getBooks(itemsPerPage: number, currentPage: number, bookName: string = ''): AxiosPromise {
        const body = {
            itemsPerPage: itemsPerPage,
            currentPage: currentPage,
            bookName: bookName
        };
        return axios.post(environment.url + '/book', body);
    }

    public getBook(id: string): AxiosPromise {
        return axios.get(environment.url + '/book/' + id);
    }

    public getBookByAuthor(id: string): AxiosPromise {
        return axios.get(environment.url + '/book/get/' + id);
    }

    public addBook(book: IBookModel): AxiosPromise {
        return axios.post(environment.url + '/book/add', book);
    }

    public updateBook(book: IBookModel, id: string): AxiosPromise {
        return axios.put(environment.url + '/book/' + id, book);
    }

    public deleteBook(id: string): AxiosPromise {
        return axios.delete(environment.url + '/book/' + id);
    }
}