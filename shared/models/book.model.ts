export interface IBookModel {
    _id: any;
    title: string;
    authors: Array<String>;
    img: string;
    magazine?: any;
}