export interface IMagazineModel {
    _id: any;
    title: string;
    publisher: string;
    img: string;
}